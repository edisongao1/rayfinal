#ifndef __MATH_COMMON__
#define __MATH_COMMON__

#include <math.h>
#include <float.h>

const float PI = float(3.141592653589793238462643383279502884197169399375);
const float INV_PI = 1.0f / PI;
const float _2PI = 2.0f * PI;
const float INV_2PI = 1.0f / _2PI;

template<class T>
inline bool fcmp_tpl(T a, T b, T fEpsilon = FLT_EPSILON)
{
	return fabs(a - b) <= fEpsilon;
}



inline float sqrt_tpl(float a) { return sqrtf(a); }
inline double sqrt_tpl(double a) { return sqrt(a); }
inline float sqrt_tpl(int i) { return sqrtf(static_cast<float>(i)); }

inline float recip_tpl(float a) { return 1.0f / a; }
inline double recip_tpl(double a) { return 1.0 / a; }
inline float recip_tpl(int i) { return 1.0f / (float)i; }

inline float fabs_tpl(float a) { return fabs(a); }
inline int fabs_tpl(int a) { return abs(a); }
inline double fabs_tpl(double a) { return fabs(a); }

float rand_float();
float rand_float(float a, float b);
int rand_int(int a, int b);

inline float radians(float angle) 
{
	static const float k = PI / 180.0f;
	return angle * k;
}

template<typename T>
inline float clamp_tpl(T x, T minx, T maxx)
{
	return std::max<T>(minx, std::min<T>(x, maxx));
}

using Rect = RECT;

//template<class T>
//using TVector3 = Eigen::Matrix<T, 3, 1>;
//
//template<class T>
//using TVector2 = Eigen::Matrix<T, 2, 1>;
//
//template<class T>
//using TVector4 = Eigen::Matrix<T, 4, 1>;
//
//template<class T>
//using TMatrix3x3 = Eigen::Matrix<T, 3, 3>;
//
//template<class T>
//using TMatrix4x4 = Eigen::Matrix<T, 4, 4>;
//
//using Vector3 = Eigen::Vector3f;
//using Vector3i = Eigen::Vector3i;
//using Vector4 = Eigen::Vector4f;
//using Vector4i = Eigen::Vector4i;
//using Vector2 = Eigen::Vector2f;
//using Vector2i = Eigen::Vector2i;
//
//using Matrix3x3 = Eigen::Matrix3f;
//using Matrix4x4 = Eigen::Matrix4f;
//
//template<class T>
//TVector4<T> Vec3ToVec4(const TVector3<T>& v, T a = 1)
//{
//	return TVector4<T>(v[0], v[1], v[2], a);
//}
//
//template<class T>
//TVector3<T> Vec4ToVec3(const TVector4<T>& v)
//{
//	return TVector3<T>(v[0], v[1], v[2]);
//}
//
//void Transform(Vector4& v, const Matrix4x4& m)
//{
//	v = m * v;
//}
//
//void Transform(Vector3& v, const Matrix4x4& m)
//{
//	v = Vec4ToVec3(m * Vec3ToVec4(v));
//}
//
//Vector4 GetTransformed(const Vector4& v, const Matrix4x4& m) 
//{
//	Vector4 v1 = m * v;
//	return v1;
//}
//
//Vector3 GetTransformed(const Vector3& v, const Matrix4x4& m)
//{
//	return Vec4ToVec3(m * Vec3ToVec4(v));
//}



#endif