/*This source code copyrighted by Lazy Foo' Productions (2004-2019)
and may not be redistributed without written permission.*/

//Using SDL and standard IO
#include "pch.h"
#include "Camera.h"
#include "RenderWindow.h"
#include "PathTracer.h"
#include "Scene.h"
#include "Transform.h"
#include "Shape.h"
#include "MeshEntity.h"
#include "MeshManager.h"
#include "SceneLoader.h"
#include "Light.h"

#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "SDL2main.lib")


//#define SCREEN_WIDTH 512
//#define SCREEN_HEIGHT 512
#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

//std::random_device rd;
//std::mt19937 gen(rd());
//std::uniform_real_distribution<float> dis(0.0f, 1.0f);

void Test();

Color RandColor() {
	return Color(rand_float(), rand_float(), rand_float(), 1);
}

//MeshEntity* CreateMeshEntity()
//{
//	Matrix4x4 worldTransform = CreateTranslation(0, -0.5f, 0);
//	worldTransform = CreateTranslation(0, 0, -8.0f) * CreateRotationX(radians(90.0f)) * CreateScaling(3.0f);
//	TriangleMesh* pMesh = MeshManager::Get().GetMesh("plane");
//	MeshEntity* pMeshEntity = new MeshEntity(pMesh, worldTransform);
//	return pMeshEntity;
//}
//
//
//Scene* CreateCornellBoxScene()
//{
//	Scene* scene = new Scene();
//	scene->mCamera.Init(Vector3(0, 0, 3),
//		Vector3(0, 0, 0),
//		Vector2i(SCREEN_WIDTH, SCREEN_HEIGHT), radians(50.0f), 0.1f, 100.0f);
//
//	TriangleMesh* pPlaneMesh = MeshManager::Get().GetMesh("plane");
//
//	// ceiling
//	{
//		Matrix4x4 worldTransform = CreateTranslation(0, 1.0f, 0) * CreateRotation(PI, 0, 0) * CreateScaling(1.5, 1, 1);
//		MeshEntity* pMeshEntity = new MeshEntity(pPlaneMesh, worldTransform);
//		scene->AddEntity(pMeshEntity);
//	}
//
//	// 
//	{
//
//	}
//
//	Sphere* sphere1 = new Sphere(1.0f, CreateTranslation(0, 0, -5.0f));
//	scene->AddEntity(sphere1);
//
//	//MeshEntity* meshEntity = CreateMeshEntity();
//	//scene->AddEntity(meshEntity);
//
//	return scene;
//}


int main(int argc, char* args[])
{
	Test();
	RenderWindow window(SCREEN_WIDTH, SCREEN_HEIGHT);
	if (!window.Init())
		return -1;

	SceneLoader loader(&window);
	Scene* scene = loader.Load("../resources/scene6.xml");
	
	SRenderSettings settings;
	settings.BlockSize = 32;
	settings.UpdateSurfaceFrequency = EUpdateSurfaceFrequency::eUpdateSurface_PerPixel;
	settings.SamplesPerPixel = 160;
	settings.BackgroundColor = Color(0, 0, 0);
	settings.MaxThreadCount = 16;

#ifdef _DEBUG
	settings.MaxThreadCount = 1;
#endif
	settings.MaxIndirectLightingDepth = 5;
	settings.IndirectLightingRayCount = 1;

	PathTracer* pPathTracer = new PathTracer(&window, scene, settings);
	pPathTracer->Render();

	window.Loop();
	return 0;
}

void Test()
{
	//UniformHemisphereSampler sampler;
	//Vector3 sum;
	//for (int i = 0; i < 10000; i++) {
	//	float pdf;
	//	Vector3 x = sampler.GetSample(&pdf);
	//	sum += x;
	//}

	//sum /= 10000.0f;
	//int a = 1;

	//SceneLoader loader;
	//float arr[4];
	//int x1 = loader.GetFloatArrayFromString("1 , 2.22, 3.54", arr, 2);


	//auto scene = loader.Load("../resources/scene1.xml");
	//return;
}
