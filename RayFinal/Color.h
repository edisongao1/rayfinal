#pragma once
class Color {
public:

	float r, g, b, a;


	Color(float r = 0, float g = 0, float b = 0, float a = 0.0)
		: r(r), g(g), b(b), a(a) { }

	inline Color operator+(const Color& rhs) const {
		return Color(r + rhs.r, g + rhs.g, b + rhs.b, a + rhs.a);
	}

	inline Color& operator+=(const Color& rhs) {
		r += rhs.r; g += rhs.g; b += rhs.b; a += rhs.a;
		return *this;
	}

	inline Color operator*(const Color& rhs) const {
		return Color(r * rhs.r, g * rhs.g, b * rhs.b, a * rhs.a);
	}

	inline Color& operator*=(const Color& rhs) {
		r *= rhs.r; g *= rhs.g; b *= rhs.b; a *= rhs.a;
		return *this;
	}

	inline Color operator*(float s) const {
		return Color(r * s, g * s, b * s, a * s);
	}

	inline Color& operator*=(float s) {
		r *= s; g *= s; b *= s; a *= s;
		return *this;
	}

	inline Color operator/(float s) const {
		float inv = 1.0f / s;
		return Color(r * inv, g * inv, b * inv, a * inv);
	}

	inline Color& operator/=(float s) {
		s = 1.0f / s;
		r *= s; g *= s; b *= s; a *= s;
		return *this;
	}

	inline bool operator==(const Color& rhs) const {
		return r == rhs.r && g == rhs.g && b == rhs.b && a == rhs.a;
	}

	inline bool operator!=(const Color& rhs) const {
		return !operator==(rhs);
	}

	inline bool IsZero() const {
		return r == 0 && g == 0 && b == 0;
	}

	inline Color Clamp()
	{
		Color c = *this;
		if (c.r < 0) c.r = 0;
		if (c.g < 0) c.g = 0;
		if (c.b < 0) c.b = 0;
		if (c.a < 0) c.a = 0;
		return c;
	}

}; // class Color


inline Color operator*(float s, const Color& c) {
	return c * s;
}

inline std::ostream& operator<<(std::ostream& os, const Color& c)
{
	os << '(' << c.r << ',' << c.g << ',' << c.b << ',' << c.a << ')';
	return os;
}
