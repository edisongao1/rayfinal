#pragma once
#include "Matrix4x4.h"

inline Matrix4x4 CreateTranslation(float x, float y, float z)
{
	return Matrix4x4(1, 0, 0, x,
		0, 1, 0, y,
		0, 0, 1, z,
		0, 0, 0, 1);
}

inline Matrix4x4 CreateRotationX(float theta)
{
	float costheta = cosf(theta);
	float sintheta = sinf(theta);
	return Matrix4x4(
		1, 0, 0, 0,
		0, costheta, -sintheta, 0,
		0, sintheta, costheta, 0,
		0, 0, 0, 1
	);
}

inline Matrix4x4 CreateRotationY(float theta)
{
	float costheta = cosf(theta);
	float sintheta = sinf(theta);
	return Matrix4x4(
		costheta, 0, sintheta, 0,
		0, 1, 0, 0,
		-sintheta, 0, costheta, 0,
		0, 0, 0, 1
	);
}

inline Matrix4x4 CreateRotationZ(float theta)
{
	float costheta = cosf(theta);
	float sintheta = sinf(theta);
	return Matrix4x4(
		costheta, -sintheta, 0, 0,
		sintheta, costheta, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	);
}

inline Matrix4x4 CreateRotation(float x, float y, float z)
{
	return CreateRotationZ(z) * CreateRotationY(y) * CreateRotationX(x);
}

inline Matrix4x4 CreateScaling(float sx, float sy, float sz) {
	return Matrix4x4(
		sx, 0, 0, 0,
		0, sy, 0, 0,
		0, 0, sz, 0,
		0, 0, 0, 1
	);
}

inline Matrix4x4 CreateScaling(float k)
{
	return Matrix4x4(
		k, 0, 0, 0,
		0, k, 0, 0,
		0, 0, k, 0,
		0, 0, 0, 1
	);
}