#pragma once
#include "Vector3.h"
#include "Color.h"

#define BRDF_EPSILON (1e-10f)

class Fresnel;
class MicrofacetDistribution;

class BxDF
{
public:
	enum eBxDF_Type
	{
		eBxDF_Transmission = (1 << 0),
		eBxDF_Reflection = (1 << 1),
		eBxDF_Specular = (1 << 2),
		eBxDF_Diffuse = (1 << 3),
		eBxDF_Glossy = (1 << 4)
	};

public:
	BxDF() {}
	//virtual float pdf(const Vector3& wo, const Vector3& wi) = 0;
	virtual Color f(const Vector3& wo, const Vector3& wi) = 0;
	virtual Color Sample_f(const Vector3& wo, Vector3* wi, float* pdf) = 0;
	virtual bool IsDelta() const = 0;
	virtual uint32_t GetType() const {
		return eBxDF_Reflection | eBxDF_Diffuse;
	}

	float CosTheta(const Vector3& w) const { return w.z; }
	float AbsCosTheta(const Vector3& w) const { return fabs(w.z); }
	float Cos2Theta(const Vector3& w) const { return w.z * w.z; }
	float Sin2Theta(const Vector3& w) const { return clamp_tpl(1.0f - Cos2Theta(w), 0.0f, 1.0f); }
	float SinTheta(const Vector3& w) const { return sqrtf(Sin2Theta(w)); };

	float Tan2Theta(const Vector3& w) const { return Sin2Theta(w) / std::max<float>(BRDF_EPSILON, Cos2Theta(w)); }
	float TanTheta(const Vector3& w) const { return sqrtf(Tan2Theta(w)); }

	float TanPhi(const Vector3& w) const
	{
		if (fcmp_tpl(w.x, 0.0f, BRDF_EPSILON)) {
			return w.y / BRDF_EPSILON;
		}
		return w.y / w.x;
	}

	float Tan2Phi(const Vector3& w) const 
	{
		float fTanPhi = TanPhi(w);
		return fTanPhi * fTanPhi;
	}

	float Cos2Phi(const Vector3& w) const
	{
		float tmp = w.x * w.x + w.y * w.y;
		if (tmp == 0.0f) {
			//tmp = BRDF_EPSILON;
			return 1.0f;
		}
		return w.x * w.x / tmp;
	}

	float Sin2Phi(const Vector3& w) const
	{
		float tmp = w.x * w.x + w.y * w.y;
		if (tmp == 0.0f) {
			//tmp = BRDF_EPSILON;
			return 0.0f;
		}
		return w.y * w.y / tmp;
	}

	float CosPhi(const Vector3& w) const 
	{
		float sintheta = SinTheta(w);
		if (sintheta == 0.0f) {
			return 1.0f;
		}
		return clamp_tpl<float>(w.x / sintheta, 0, 1);
	}

	float SinPhi(const Vector3& w) const
	{
		float sintheta = SinTheta(w);
		if (sintheta == 0.0f) {
			return 0.0f;
		}
		return clamp_tpl<float>(w.y / sintheta, 0, 1);
	}
};

class LambertianBRDF : public BxDF
{
public:
	LambertianBRDF(const Color& reflection) 
		: mReflection(reflection)
	{
		
	}

	virtual Color f(const Vector3& wo, const Vector3& wi) override;
	virtual Color Sample_f(const Vector3& wo, Vector3* wi, float* pdf) override;
	virtual bool IsDelta() const { return false; }
	virtual uint32_t GetType() const override {
		return eBxDF_Reflection | eBxDF_Diffuse;
	}

private:
	Color		mReflection;
};

class SpecularBRDF : public BxDF
{
public:
	SpecularBRDF(const Color& R, Fresnel* fresnel)
		:mR(R), mFresnel(fresnel)
	{

	}
	virtual Color f(const Vector3& wo, const Vector3& wi) override;
	virtual Color Sample_f(const Vector3& wo, Vector3* wi, float* pdf) override;
	virtual bool IsDelta() const { return true; }
	virtual uint32_t GetType() const override { return eBxDF_Reflection | eBxDF_Specular; }

private:
	Color		mR;
	Fresnel*	mFresnel;
};

class SpecularBTDF : public BxDF
{
public:
	SpecularBTDF(const Color& T, float etai, float etaT, Fresnel* fresnel)
		:mT(T), mFresnel(fresnel), mEtaI(etai), mEtaT(etaT)
	{

	}

	virtual Color f(const Vector3& wo, const Vector3& wi) override;
	virtual Color Sample_f(const Vector3& wo, Vector3* wi, float* pdf) override;
	virtual bool IsDelta() const { return true; }
	virtual uint32_t GetType() const override { return eBxDF_Transmission | eBxDF_Specular; }
private:
	Color		mT;
	float		mEtaI, mEtaT;
	Fresnel*	mFresnel;
};

class TorranceSparrowBRDF : public BxDF
{
public:
	TorranceSparrowBRDF(const Color& R, MicrofacetDistribution* distribution, Fresnel* fresnel)
		:mR(R), mDistribution(distribution), mFresnel(fresnel)
	{

	}
	virtual Color f(const Vector3& wo, const Vector3& wi) override;
	virtual Color Sample_f(const Vector3& wo, Vector3* wi, float* pdf) override;
	virtual bool IsDelta() const { return false; }
	virtual uint32_t GetType() const override { return eBxDF_Reflection | eBxDF_Diffuse; }

private:
	Color						mR;
	MicrofacetDistribution*		mDistribution;
	Fresnel*					mFresnel;
};

class OrenNayarBRDF : public BxDF
{
public:
	OrenNayarBRDF(const Color& R, float sigma);
	virtual Color f(const Vector3& wo, const Vector3& wi) override;
	virtual Color Sample_f(const Vector3& wo, Vector3* wi, float* pdf) override;
	virtual bool IsDelta() const { return false; }
	virtual uint32_t GetType() const override { return eBxDF_Reflection | eBxDF_Diffuse; }
private:
	Color						mR;
	float						mSigma;
	float						A, B;
};
