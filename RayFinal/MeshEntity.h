#pragma once
#include "Shape.h"

class TriangleMesh;
class MeshEntity;
struct SUVCoordinate;


class Triangle : public IPrimitive
{
public:
	void Init(MeshEntity* mEntity, int* indices);
	virtual bool RayIntersect(const Ray& ray, DifferentialGeometry& result) override;
	virtual bool RayIntersectFast(const Ray& ray, float& t) override;
	virtual ISceneEntity* GetSceneEntity() override;

	int* mIndices;
	MeshEntity* mEntity;

	Vector3		ns, ts, bs;
	Vector3		dpdu, dpdv;

};

class MeshEntity : public ISceneEntity
{
public:	
	MeshEntity(TriangleMesh* mesh, const Matrix4x4& worldTransform);
	
	virtual void AddToScene(Scene* scene);
	virtual ~MeshEntity();
	
private:
	//void InitVertexBuffers(const Vector3* positions, const Vector3* normals);

public:
	Vector3*			mPositions;
	Vector3*			mNormals;
	SUVCoordinate*		mUVs;
	int					mVertexCount;

	int*				mIndices;
	int					mIndiceCount;

	Triangle*			mTriangles;

	Matrix4x4			mNormalObjectToWorld;

	

};