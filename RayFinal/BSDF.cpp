#include "pch.h"
#include "BSDF.h"
#include "Sampler.h"
#include "Fresnel.h"
#include "MicrofacetDistribution.h"

Color LambertianBRDF::f(const Vector3& wo, const Vector3& wi)
{
	return mReflection * INV_PI;
}

Color LambertianBRDF::Sample_f(const Vector3& wo, Vector3* wi, float* pdf)
{
	*wi = CosineWeightedHemisphereSampler().GetSample(pdf);
	return f(wo, *wi);
}

Color SpecularBRDF::f(const Vector3& wo, const Vector3& wi)
{
	return Color(0, 0, 0, 0);
}

Color SpecularBRDF::Sample_f(const Vector3& wo, Vector3* wi, float* pdf)
{
	*wi = Vector3(-wo.x, -wo.y, wo.z);
	*pdf = 1.0f;
	float cosi = AbsCosTheta(*wi);
	return mR * mFresnel->Evaluate(cosi) / max(cosi, BRDF_EPSILON);
}

Color SpecularBTDF::f(const Vector3& wo, const Vector3& wi)
{
	return Color(0);
}

Color SpecularBTDF::Sample_f(const Vector3& wo, Vector3* wi, float* pdf)
{
	*pdf = 1.0f;

	// refract
	bool entering = wo.z > 0;
	float etai = entering ? mEtaI : mEtaT;
	float etat = entering ? mEtaT : mEtaI;

	float sin2i = Sin2Theta(wo);
	float sin2t = (etai * etai) * sin2i / (etat * etat);
	
	// ȫ����
	if (sin2t >= 1.0f)
	{
		return Color(0);
	}

	float sini = sqrtf(sin2i);
	float sint = sqrtf(sin2t);

	float cosphii = (sini == 0.0f) ? 1.0f : wo.x / sini;
	float sinphii = (sini == 0.0f) ? 0.0f: wo.y / sini;

	float cost = sqrtf(1.0f - sin2t);
	wi->z = entering ? -cost : cost;
	wi->x = -sint * cosphii;
	wi->y = -sint * sinphii;
	
	float fr = 1.0f - mFresnel->Evaluate(CosTheta(*wi));
	if (fr < 0)
		fr = 0;

	return mT * fr / max(AbsCosTheta(*wi), BRDF_EPSILON);
}

Color TorranceSparrowBRDF::f(const Vector3& wo, const Vector3& wi)
{
	float cosi = AbsCosTheta(wi);
	float coso = AbsCosTheta(wo);
	if (cosi == 0 || coso == 0)
		return Color(0);

	Vector3 wh = (wo + wi).GetNormalized();

	float cosh = wo.Dot(wh);
	float D = mDistribution->D(wh);
	float F = mFresnel->Evaluate(cosh);
	float G = mDistribution->G(wi, wo, wh);

	return mR * D * F * G / (4 * cosi * coso);
}

Color TorranceSparrowBRDF::Sample_f(const Vector3& wo, Vector3* wi, float* pdf)
{
	mDistribution->Sample_D(wo, wi, pdf);
	return f(wo, *wi);
}

OrenNayarBRDF::OrenNayarBRDF(const Color& R, float sigma)
	:mR(R), mSigma(sigma)
{
	float sigma2 = sigma * sigma;
	A = 1.0f - sigma2 / (2 * (sigma2 + 0.33f));
	B = 0.45 * sigma2 / (sigma2 + 0.09);
}

Color OrenNayarBRDF::f(const Vector3& wo, const Vector3& wi)
{
	float cosI = AbsCosTheta(wi);
	float cosO = AbsCosTheta(wo);
	float sinI = SinTheta(wi);
	float sinO = SinTheta(wo);

	float cosPhiO = (sinO == 0) ? 1.0f : wo.x / sinO;
	float sinPhiO = (sinO == 0) ? 0.0f : wo.y / sinO;
	float cosPhiI = (sinI == 0) ? 1.0f : wi.x / sinI;
	float sinPhiI = (sinI == 0) ? 0.0f : wi.y / sinI;
	float cosPhiDelta = cosPhiI * cosPhiO + sinPhiI * sinPhiO;

	float sinAlpha, tanBeta;
	
	// thetaI > thetaO
	if (cosI < cosO)
	{
		sinAlpha = sinI;
		tanBeta = sinO / max(1e-10, cosO);
	}
	else
	{
		sinAlpha = sinO;
		tanBeta = sinI / max(1e-10, cosI);
	}

	return mR * INV_PI * (A + B * max(0, cosPhiDelta) * sinAlpha * tanBeta);
}

Color OrenNayarBRDF::Sample_f(const Vector3& wo, Vector3* wi, float* pdf)
{
	*wi = CosineWeightedHemisphereSampler().GetSample(pdf);
	return f(wo, *wi);
}

