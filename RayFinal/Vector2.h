#pragma once
#include "MathCommon.h"
#include <math.h>

template<class T>
class TVector2
{
public:
	const static int DIMENSION = 2;

	T x, y;

	TVector2()
	{
		x = 0;
		y = 0;
	}

	TVector2& operator=(const TVector2& t)
	{
		x = t.x;
		y = t.y;
		return *this;
	}

	TVector2(T x, T y) :x(x), y(y) {}

	bool operator==(const TVector2& t) const {
		return x == t.x && y == t.y;
	}

	bool operator !=(const TVector2 &a) const {
		return x != a.x || y != a.y;
	}

	TVector2 operator -() const { return TVector2(-x, -y); }

	TVector2 operator +(T k) const {
		return TVector2(x + k, y + k);
	}
	TVector2 operator +(const TVector2 &a) const {
		return TVector2(x + a.x, y + a.y);
	}

	TVector2 operator -(T k) const {
		return TVector2(x - k, y - k);
	}
	TVector2 operator -(const TVector2 &a) const {
		return TVector2(x - a.x, y - a.y);
	}

	TVector2 operator *(T k) const {
		return TVector2(x*k, y*k);
	}

	TVector2 operator *(const TVector2 &a) const {
		return TVector2(x * a.x, y * a.y);
	}

	TVector2 operator /(T a) const {
		auto oneOverA = recip_tpl(a);
		return TVector2<T>(x*oneOverA, y*oneOverA);
	}

	TVector2& operator +=(const TVector2 &a) {
		x += a.x; y += a.y;
		return *this;
	}
	TVector2& operator += (T k) {
		x += k; y += k;
		return *this;
	}
	TVector2& operator -=(const TVector2 &a) {
		x -= a.x; y -= a.y;
		return *this;
	}
	TVector2& operator -= (T k) {
		x -= k; y -= k;
		return *this;
	}

	TVector2& operator *=(T a) {
		x *= a; y *= a;
		return *this;
	}

	TVector2& operator /=(T a) {
		auto oneOverA = recip_tpl(a);
		x *= oneOverA; y *= oneOverA;
		return *this;
	}

	void Normalize() {
		T magSq = LengthSquared();
		if (magSq > 0) { 
			// check for divide-by-zero
			T oneOverMag = recip_tpl(sqrt_tpl(magSq));
			x *= oneOverMag;
			y *= oneOverMag;
		}
	}

	TVector2 GetNormalized() {
		TVector2<T> v(*this);
		v.Normalize();
		return v;
	}

	auto Length() const {
		return sqrt_tpl(x * x + y * y);
	}

	T LengthSquared() const {
		return x * x + y * y;
	}

	T Dot(const TVector2& a) const {
		return x * a.x + y * a.y;
	}

	auto Distance(const TVector2 &b) const {
		T dx = x - b.x;
		T dy = y - b.y;
		return sqrt(dx*dx + dy * dy);
	}

	T DistanceSquared(const TVector2& b) const {
		T dx = x - b.x;
		T dy = y - b.y;
		return (dx*dx + dy * dy);
	}
};

template<class T>
inline T Dot(const TVector2<T> &a, const TVector2<T> &b)
{
	return a.Dot(b);
}

template<>
inline bool TVector2<float>::operator==(const TVector2<float>& t) const {
	return fcmp_tpl(x, t.x) && fcmp_tpl(y, t.y);
}

template<>
inline bool TVector2<double>::operator==(const TVector2<double>& t) const {
	return fcmp_tpl(x, t.x) && fcmp_tpl(y, t.y);
}

template<>
inline bool TVector2<float>::operator!=(const TVector2<float>& t) const {
	return !fcmp_tpl(x, t.x) || !fcmp_tpl(y, t.y);
}

template<>
inline bool TVector2<double>::operator!=(const TVector2<double>& t) const {
	return !fcmp_tpl(x, t.x) || !fcmp_tpl(y, t.y);
}

using Vector2 = TVector2<float>;
using Vector2f = TVector2<float>;
using Vector2i = TVector2<int>;
using Vector2d = TVector2<double>;

