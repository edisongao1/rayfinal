#include "pch.h"
#include "Scene.h"
#include "Camera.h"


Scene::Scene()
{
	
}

ISceneEntity* Scene::RayIntersect(const Ray& ray, DifferentialGeometry& result)
{
	Ray r(ray);
	ISceneEntity* pHitEntity = nullptr;
	for (auto it : mObjects) {
		if (it->RayIntersect(r, result)) {
			r.maxt = result.t;
			//pHitEntity = it->Get;
			pHitEntity = it->GetSceneEntity();
		}
	}
	return pHitEntity;
}

bool Scene::RayIntersectFast(const Ray& ray)
{
	float t;
	for (auto it : mObjects) {
		if (it->RayIntersectFast(ray, t)) {
			return true;
		}
	}
	return false;
}

void Scene::AddEntity(ISceneEntity* pObject)
{
	pObject->AddToScene(this);
}

void Scene::AddLight(ILight* pLight)
{
	mLights.push_back(pLight);
}

