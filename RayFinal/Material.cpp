#include "pch.h"
#include "Common.h"
#include "Material.h"
#include "MemoryPool.h"
#include "Fresnel.h"
#include "MicrofacetDistribution.h"

BSDF::BSDF()
	:mSize(0)
{
	memset(mBxDFs, 0, sizeof(mBxDFs));
}

void BSDF::SampleBSDF(const Vector3& wo, Vector3* wi, float* pdf)
{
	// TODO:
	mBxDFs[0]->Sample_f(wo, wi, pdf);
}

Color BSDF::f(const Vector3& wo, const Vector3& wi)
{
	// TODO:
	return mBxDFs[0]->f(wo, wi);
}

void BSDF::AddBxDF(BxDF* bxdf)
{
	mBxDFs[mSize++] = bxdf;
}

LambertianMaterial::LambertianMaterial(const Color& diffuseColor)
	:mDiffuseColor(diffuseColor)
{
	//BSDF* bsdf = new BSDF;
	////BSDF* bsdf = CreateObject<BSDF>();
	//bsdf->AddBxDF(new LambertianBRDF(mDiffuseColor));
	//mBrdf = bsdf;
	mBrdf = new LambertianBRDF(mDiffuseColor);
}



//BSDF* LambertianMaterial::GetBSDF(const DifferentialGeometry& dg)
//{
//	//BSDF* bsdf = new BSDF;
//	//BSDF* bsdf = CreateObject<BSDF>();
//	//bsdf->AddBxDF(new LambertianBRDF(mDiffuseColor));
//	//return bsdf;
//	return mBsdf;
//}
//
//BSDF* PureColorMaterial::GetBSDF(const DifferentialGeometry& dg)
//{
//	return NULL;
//}

SpecularDielectricMaterial::SpecularDielectricMaterial(const Color& R, const Color& T, float etai, float etat)
{
	mFresnel = new FresnelDielectric(etai, etat);
	mBrdf = new SpecularBRDF(R, mFresnel);
	mBtdf = new SpecularBTDF(T, etai, etat, mFresnel);
}

SpecularDielectricMaterial::~SpecularDielectricMaterial()
{
	SAFE_DELETE(mBrdf);
	SAFE_DELETE(mBtdf);
	SAFE_DELETE(mFresnel);
}

SpecularConductorMaterial::SpecularConductorMaterial(const Color& R, float eta, float k)
{
	mFresnel = new FresnelConductor(eta, k);
	mBrdf = new SpecularBRDF(R, mFresnel);
}

SpecularConductorMaterial::~SpecularConductorMaterial()
{
	SAFE_DELETE(mBrdf);
	SAFE_DELETE(mFresnel);
}

MicrofacetMaterial::MicrofacetMaterial(const Color& R, float eta, float k, float e)
{
	mDistribution = new BlinnDistribution(e);
	mFresnel = new FresnelConductor(eta, k);
	mBrdf = new TorranceSparrowBRDF(R, mDistribution, mFresnel);
}

MicrofacetMaterial::~MicrofacetMaterial()
{
	SAFE_DELETE(mBrdf);
	SAFE_DELETE(mFresnel);
	SAFE_DELETE(mDistribution);
}

