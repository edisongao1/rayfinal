#pragma once
#include <SDL.h>
#include "MathCommon.h"
#include "Color.h"
#include "Vector2.h"

class RenderWindow
{
public:
	RenderWindow(int width, int height, bool EnableGamma = true);
	~RenderWindow();
	bool Init();
	int GetWidth() const { return m_nWidth; }
	int GetHeight() const { return m_nHeight; }
	Vector2i GetSize() const { return Vector2i(m_nWidth, m_nHeight); }

	void SetPixels(Color* data, Rect region);
	void SetPixels(Color* data);
	void SetPixel(Color color, int x, int y);

	void Loop();

private:
	void DrawFrame();
	Uint32 ConvertColorToUint32(const Color& c);

	int		m_nWidth;
	int		m_nHeight;
	bool	m_bQuit;
	bool	m_bEnableGamma;


	Uint32*			m_pPixelsData;
	std::mutex		m_pixelDataMutex;

	//The window we'll be rendering to
	SDL_Window*		m_pSDLWindow;
	//The surface contained by the window
	SDL_Surface*	m_pScreenSurface;
	// The surface to be drawn on
	SDL_Surface*	m_pDrawableSurface;

	

};

