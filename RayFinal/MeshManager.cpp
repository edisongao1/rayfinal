#include "pch.h"
#include "MeshManager.h"

MeshManager::MeshManager()
	:mPlaneMesh(nullptr)
{
	PreloadMeshes();
}

MeshManager& MeshManager::Get()
{
	static MeshManager instance;
	return instance;
}

void MeshManager::PreloadMeshes()
{
	CreatePlaneMesh();
}

TriangleMesh* MeshManager::GetMesh(const std::string& name)
{
	auto it = mMeshMap.find(name);
	if (it != mMeshMap.end()) {
		return it->second;
	}
	return nullptr;
}

void MeshManager::CreatePlaneMesh()
{
	auto pPlaneMesh = new TriangleMesh("plane");

	Vector3* positions = new Vector3[4];
	Vector3* normals = new Vector3[4];
	positions[0] = Vector3(-1, 0, 1);
	positions[1] = Vector3(1, 0, 1);
	positions[2] = Vector3(1, 0, -1);
	positions[3] = Vector3(-1, 0, -1);

	normals[0] = Vector3(0, 1, 0);
	normals[1] = Vector3(0, 1, 0);
	normals[2] = Vector3(0, 1, 0);
	normals[3] = Vector3(0, 1, 0);

	int* indices = new int[6];
	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;

	indices[3] = 0;
	indices[4] = 2;
	indices[5] = 3;

	pPlaneMesh->mPositions = positions;
	pPlaneMesh->mNormals = normals;
	pPlaneMesh->mVertexCount = 4;
	pPlaneMesh->mIndices = indices;
	pPlaneMesh->mIndiceCount = 6;

	mMeshMap.insert(std::make_pair(pPlaneMesh->GetName(), pPlaneMesh));
}

TriangleMesh* MeshManager::CreatePlaneMesh(const std::string& name, Vector2 size)
{
	auto it = mMeshMap.find(name);
	if (it != mMeshMap.end()) {
		TriangleMesh* mesh = it->second;
		Vector3 pos = mesh->mPositions[0];
		if (!fcmp_tpl(pos.x * 2, size.x) || !fcmp_tpl(pos.y * 2, size.y)) {
			return nullptr;
		}
		return mesh;
	}

	auto pPlaneMesh = new TriangleMesh(name);

	Vector3* positions = new Vector3[4];
	Vector3* normals = new Vector3[4];
	positions[0] = Vector3(-size.x * 0.5f, 0, size.y * 0.5f);
	positions[1] = Vector3(size.x * 0.5f, 0, size.y * 0.5f);
	positions[2] = Vector3(size.x * 0.5f, 0, -size.y * 0.5f);
	positions[3] = Vector3(-size.x * 0.5f, 0, -size.y * 0.5f);

	normals[0] = Vector3(0, 1, 0);
	normals[1] = Vector3(0, 1, 0);
	normals[2] = Vector3(0, 1, 0);
	normals[3] = Vector3(0, 1, 0);

	int* indices = new int[6];
	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;

	indices[3] = 0;
	indices[4] = 2;
	indices[5] = 3;

	SUVCoordinate* uvs = new SUVCoordinate[4];
	uvs[0].u = 0.0f; uvs[0].v = 0.0f;
	uvs[1].u = 1.0f; uvs[1].v = 0.0f;
	uvs[2].u = 1.0f; uvs[2].v = 1.0f;
	uvs[3].u = 0.0f; uvs[3].v = 1.0f;

	pPlaneMesh->mPositions = positions;
	pPlaneMesh->mNormals = normals;
	pPlaneMesh->mVertexCount = 4;
	pPlaneMesh->mIndices = indices;
	pPlaneMesh->mIndiceCount = 6;
	pPlaneMesh->mUVs = uvs;

	mMeshMap.insert(std::make_pair(pPlaneMesh->GetName(), pPlaneMesh));
	return pPlaneMesh;
}
