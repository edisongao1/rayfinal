#include "pch.h"
#include "Light.h"


Color HemisphereLight::Sample_L(const Vector3& p, Vector3* wi, float* dist, float* pdf)
{
	*wi = mSampler.GetSample(pdf);
	*dist = mLightRange;
	return mRadiance;
}

PlaneDiffuseAreaLight::PlaneDiffuseAreaLight(const Matrix4x4& world, const Vector2& dim, 
	const Color& radiance) 
	:mObjectToWorld(world), mDimensions(dim), mRadiance(radiance)
{
	mArea = dim.x * dim.y;
	mNs = TransformDirection(Vector3(0, 1, 0), mObjectToWorld);
}

Color PlaneDiffuseAreaLight::Sample_L(const Vector3& p, Vector3* wi, float* dist, float* pdf)
{
	Vector3 localPos;
	localPos.x = rand_float(-0.5f, 0.5f) * mDimensions.x;
	localPos.z = rand_float(-0.5f, 0.5f) * mDimensions.y;
	localPos.y = 0;

	Vector3 worldPos = TransformPoint(localPos, mObjectToWorld);
	*wi = worldPos - p;
	float distSq = (*wi).LengthSquared();
	*dist = sqrtf(distSq);
	*wi = (*wi) / (*dist);

	float costheta = (*wi).Dot(mNs);
	*pdf = distSq / (fabs(costheta) * mArea);

	if (costheta > 0)
		return Color(0, 0, 0, 0);
	return mRadiance;
}

DiskDiffuseAreaLight::DiskDiffuseAreaLight(const Matrix4x4& worldTransform, float radius, const Color& radiance)
	:mObjectToWorld(worldTransform), mRadius(radius), mRadiance(radiance)
	,mSampler(radius)
{
	mNs = TransformDirection(Vector3(0, 0, 1.0f), mObjectToWorld);
	mArea = PI * mRadius * mRadius;
}

Color DiskDiffuseAreaLight::Sample_L(const Vector3& p, Vector3* wi, float* dist, float* pdf)
{
	float pdfL;
	Vector3 localPos = mSampler.GetSample(&pdfL);
	Vector3 worldPos = TransformPoint(localPos, mObjectToWorld);

	Vector3 d = worldPos - p;
	float distSq = d.LengthSquared();
	*dist = sqrtf(distSq);

	*wi = d / (*dist);
	
	float costheta = Dot(*wi, mNs);
	if (costheta == 0) {
		*pdf = 1.0f;
	}
	else {
		*pdf = pdfL * distSq / fabs(costheta);
	}

	if (costheta > 0)
		return Color(0);

	return mRadiance;
}

Color PointLight::Sample_L(const Vector3& p, Vector3* wi, float* dist, float* pdf)
{
	Vector3 d = mPosition - p;

	float distSq = d.LengthSquared();
	*dist = sqrtf(distSq);

	*wi = d / (*dist);
	*pdf = 1.0f;

	return mIntensity / (distSq);
}

