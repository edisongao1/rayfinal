#pragma once

#include "Matrix3x3.h"
#include "Vector4.h"

template<class F>
class TMatrix4x4 {
public:
	TMatrix4x4(void)
	{

	}

	TMatrix4x4(F * data)
	{
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				(*this)(i, j) = data[i * 4 + j];
			}
		}
	}

	template<class F1, class F2, class F3, class F4,
		class F5, class F6, class F7, class F8, 
		class F9, class F10, class F11, class F12, 
		class F13, class F14, class F15, class F16>
		explicit TMatrix4x4(F1 v00, F2 v01, F3 v02, F4 v03,
			F5 v10, F6 v11, F7 v12, F8 v13,
			F9 v20, F10 v21, F11 v22, F12 v23,
			F13 v30, F14 v31, F15 v32, F16 v33)
	{
		m00 = v00; m01 = v01; m02 = v02; m03 = v03;
		m10 = v10; m11 = v11; m12 = v12; m13 = v13;
		m20 = v20; m21 = v21; m22 = v22; m23 = v23;
		m30 = v30; m31 = v31; m32 = v32; m33 = v33;
	}


	template<class F1> 
	TMatrix4x4<F>(const TMatrix3x3<F1>& m)
	{
		m00 = F(m.m00);	m01 = F(m.m01);	m02 = F(m.m02);	m03 = 0;
		m10 = F(m.m10);	m11 = F(m.m11);	m12 = F(m.m12);	m13 = 0;
		m20 = F(m.m20);	m21 = F(m.m21);	m22 = F(m.m22);	m23 = 0;
		m30 = 0;		m31 = 0;		m32 = 0;		m33 = 1;
	}


	F& operator()(int i, int j)
	{
		return m_entries[i][j];
	}

	F operator()(int i, int j) const
	{
		return m_entries[i][j];
	}

	inline static TMatrix4x4<F> Identity()
	{
		static TMatrix4x4<F> t(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1);
		return t;
	}
	inline static TMatrix4x4<F> Zero()
	{
		static TMatrix4x4<F> t(
			0, 0, 0, 0, 
			0, 0, 0, 0, 
			0, 0, 0, 0, 
			0, 0, 0, 0);
		return t;
	}

	void Invert()
	{
		F	tmp[12];
		TMatrix4x4<F>	m = *this;

		tmp[0] = m.m22 * m.m33;
		tmp[1] = m.m32 * m.m23;
		tmp[2] = m.m12 * m.m33;
		tmp[3] = m.m32 * m.m13;
		tmp[4] = m.m12 * m.m23;
		tmp[5] = m.m22 * m.m13;
		tmp[6] = m.m02 * m.m33;
		tmp[7] = m.m32 * m.m03;
		tmp[8] = m.m02 * m.m23;
		tmp[9] = m.m22 * m.m03;
		tmp[10] = m.m02 * m.m13;
		tmp[11] = m.m12 * m.m03;

		m00 = tmp[0] * m.m11 + tmp[3] * m.m21 + tmp[4] * m.m31;
		m00 -= tmp[1] * m.m11 + tmp[2] * m.m21 + tmp[5] * m.m31;
		m01 = tmp[1] * m.m01 + tmp[6] * m.m21 + tmp[9] * m.m31;
		m01 -= tmp[0] * m.m01 + tmp[7] * m.m21 + tmp[8] * m.m31;
		m02 = tmp[2] * m.m01 + tmp[7] * m.m11 + tmp[10] * m.m31;
		m02 -= tmp[3] * m.m01 + tmp[6] * m.m11 + tmp[11] * m.m31;
		m03 = tmp[5] * m.m01 + tmp[8] * m.m11 + tmp[11] * m.m21;
		m03 -= tmp[4] * m.m01 + tmp[9] * m.m11 + tmp[10] * m.m21;
		m10 = tmp[1] * m.m10 + tmp[2] * m.m20 + tmp[5] * m.m30;
		m10 -= tmp[0] * m.m10 + tmp[3] * m.m20 + tmp[4] * m.m30;
		m11 = tmp[0] * m.m00 + tmp[7] * m.m20 + tmp[8] * m.m30;
		m11 -= tmp[1] * m.m00 + tmp[6] * m.m20 + tmp[9] * m.m30;
		m12 = tmp[3] * m.m00 + tmp[6] * m.m10 + tmp[11] * m.m30;
		m12 -= tmp[2] * m.m00 + tmp[7] * m.m10 + tmp[10] * m.m30;
		m13 = tmp[4] * m.m00 + tmp[9] * m.m10 + tmp[10] * m.m20;
		m13 -= tmp[5] * m.m00 + tmp[8] * m.m10 + tmp[11] * m.m20;

		tmp[0] = m.m20*m.m31;
		tmp[1] = m.m30*m.m21;
		tmp[2] = m.m10*m.m31;
		tmp[3] = m.m30*m.m11;
		tmp[4] = m.m10*m.m21;
		tmp[5] = m.m20*m.m11;
		tmp[6] = m.m00*m.m31;
		tmp[7] = m.m30*m.m01;
		tmp[8] = m.m00*m.m21;
		tmp[9] = m.m20*m.m01;
		tmp[10] = m.m00*m.m11;
		tmp[11] = m.m10*m.m01;

		m20 = tmp[0] * m.m13 + tmp[3] * m.m23 + tmp[4] * m.m33;
		m20 -= tmp[1] * m.m13 + tmp[2] * m.m23 + tmp[5] * m.m33;
		m21 = tmp[1] * m.m03 + tmp[6] * m.m23 + tmp[9] * m.m33;
		m21 -= tmp[0] * m.m03 + tmp[7] * m.m23 + tmp[8] * m.m33;
		m22 = tmp[2] * m.m03 + tmp[7] * m.m13 + tmp[10] * m.m33;
		m22 -= tmp[3] * m.m03 + tmp[6] * m.m13 + tmp[11] * m.m33;
		m23 = tmp[5] * m.m03 + tmp[8] * m.m13 + tmp[11] * m.m23;
		m23 -= tmp[4] * m.m03 + tmp[9] * m.m13 + tmp[10] * m.m23;
		m30 = tmp[2] * m.m22 + tmp[5] * m.m32 + tmp[1] * m.m12;
		m30 -= tmp[4] * m.m32 + tmp[0] * m.m12 + tmp[3] * m.m22;
		m31 = tmp[8] * m.m32 + tmp[0] * m.m02 + tmp[7] * m.m22;
		m31 -= tmp[6] * m.m22 + tmp[9] * m.m32 + tmp[1] * m.m02;
		m32 = tmp[6] * m.m12 + tmp[11] * m.m32 + tmp[3] * m.m02;
		m32 -= tmp[10] * m.m32 + tmp[2] * m.m02 + tmp[7] * m.m12;
		m33 = tmp[10] * m.m22 + tmp[4] * m.m02 + tmp[9] * m.m12;
		m33 -= tmp[8] * m.m12 + tmp[11] * m.m22 + tmp[5] * m.m02;

		F det = (m.m00*m00 + m.m10*m01 + m.m20*m02 + m.m30*m03);

		F idet = (F)recip_tpl(det);
		m00 *= idet; m01 *= idet; m02 *= idet; m03 *= idet;
		m10 *= idet; m11 *= idet; m12 *= idet; m13 *= idet;
		m20 *= idet; m21 *= idet; m22 *= idet; m23 *= idet;
		m30 *= idet; m31 *= idet; m32 *= idet; m33 *= idet;
	}

	TMatrix4x4<F> GetInverted() const
	{
		TMatrix4x4<F> dst = *this; dst.Invert(); return dst;
	}

	float Determinant() const
	{
		return (m00*m11*m22) + (m01*m12*m20) + (m02*m10*m21)
			- (m02*m11*m20) - (m00*m12*m21) - (m01*m10*m22);
	}

	void Transpose()
	{
		TMatrix4x4<F> tmp = *this;
		m00 = tmp.m00; m01 = tmp.m10; m02 = tmp.m20; m03 = tmp.m30;
		m10 = tmp.m01; m11 = tmp.m11; m12 = tmp.m21; m13 = tmp.m31;
		m20 = tmp.m02; m21 = tmp.m12; m22 = tmp.m22; m23 = tmp.m32;
		m30 = tmp.m03; m31 = tmp.m13; m32 = tmp.m23; m33 = tmp.m33;
	}

	TMatrix4x4<F> GetTransposed() const
	{
		TMatrix4x4<F> tmp;
		tmp.m00 = m00; tmp.m01 = m10;	tmp.m02 = m20; tmp.m03 = m30;
		tmp.m10 = m01; tmp.m11 = m11;	tmp.m12 = m21; tmp.m13 = m31;
		tmp.m20 = m02; tmp.m21 = m12;	tmp.m22 = m22; tmp.m23 = m32;
		tmp.m30 = m03; tmp.m31 = m13;	tmp.m32 = m23; tmp.m33 = m33;
		return tmp;
	}

public:
	union
	{
		struct
		{
			F m00, m01, m02, m03;
			F m10, m11, m12, m13;
			F m20, m21, m22, m23;
			F m30, m31, m32, m33;
		};

		F m_entries[4][4];
	};
};

using Matrix4x4 = TMatrix4x4<float>;

template<class F>
inline TMatrix4x4<F> operator+(const TMatrix4x4<F>& mm0, const TMatrix4x4<F>& mm1)
{
	TMatrix4x4<F> r;
	r.m00 = mm0.m00 + mm1.m00;	r.m01 = mm0.m01 + mm1.m01;	r.m02 = mm0.m02 + mm1.m02;	r.m03 = mm0.m03 + mm1.m03;
	r.m10 = mm0.m10 + mm1.m10;	r.m11 = mm0.m11 + mm1.m11;	r.m12 = mm0.m12 + mm1.m12;	r.m13 = mm0.m13 + mm1.m13;
	r.m20 = mm0.m20 + mm1.m20;	r.m21 = mm0.m21 + mm1.m21;	r.m22 = mm0.m22 + mm1.m22;	r.m23 = mm0.m23 + mm1.m23;
	r.m30 = mm0.m30 + mm1.m30;	r.m31 = mm0.m31 + mm1.m31;	r.m32 = mm0.m32 + mm1.m32;	r.m33 = mm0.m33 + mm1.m33;
	return r;
}

template<class F1, class F2>
inline TMatrix4x4<F1> operator*(const TMatrix4x4<F1>& l, const TMatrix4x4<F2>& r)
{
	TMatrix4x4<F1> res;
	res.m00 = l.m00*r.m00 + l.m01*r.m10 + l.m02*r.m20 + l.m03*r.m30;
	res.m10 = l.m10*r.m00 + l.m11*r.m10 + l.m12*r.m20 + l.m13*r.m30;
	res.m20 = l.m20*r.m00 + l.m21*r.m10 + l.m22*r.m20 + l.m23*r.m30;
	res.m30 = l.m30*r.m00 + l.m31*r.m10 + l.m32*r.m20 + l.m33*r.m30;
	res.m01 = l.m00*r.m01 + l.m01*r.m11 + l.m02*r.m21 + l.m03*r.m31;
	res.m11 = l.m10*r.m01 + l.m11*r.m11 + l.m12*r.m21 + l.m13*r.m31;
	res.m21 = l.m20*r.m01 + l.m21*r.m11 + l.m22*r.m21 + l.m23*r.m31;
	res.m31 = l.m30*r.m01 + l.m31*r.m11 + l.m32*r.m21 + l.m33*r.m31;
	res.m02 = l.m00*r.m02 + l.m01*r.m12 + l.m02*r.m22 + l.m03*r.m32;
	res.m12 = l.m10*r.m02 + l.m11*r.m12 + l.m12*r.m22 + l.m13*r.m32;
	res.m22 = l.m20*r.m02 + l.m21*r.m12 + l.m22*r.m22 + l.m23*r.m32;
	res.m32 = l.m30*r.m02 + l.m31*r.m12 + l.m32*r.m22 + l.m33*r.m32;
	res.m03 = l.m00*r.m03 + l.m01*r.m13 + l.m02*r.m23 + l.m03*r.m33;
	res.m13 = l.m10*r.m03 + l.m11*r.m13 + l.m12*r.m23 + l.m13*r.m33;
	res.m23 = l.m20*r.m03 + l.m21*r.m13 + l.m22*r.m23 + l.m23*r.m33;
	res.m33 = l.m30*r.m03 + l.m31*r.m13 + l.m32*r.m23 + l.m33*r.m33;
	return res;
}

template<class F1, class F2>
inline TVector4<F1> operator*(const TMatrix4x4<F2> &m, const TVector4<F1> &v)
{
	return TVector4<F1>(v.x*m.m00 + v.y*m.m01 + v.z*m.m02 + v.w*m.m03,
		v.x*m.m10 + v.y*m.m11 + v.z*m.m12 + v.w*m.m13,
		v.x*m.m20 + v.y*m.m21 + v.z*m.m22 + v.w*m.m23,
		v.x*m.m30 + v.y*m.m31 + v.z*m.m32 + v.w*m.m33);
}

template<class F1, class F2>
inline TVector4<F1> operator*(const TVector4<F1> &v, const TMatrix4x4<F2> &m)
{
	return TVector4<F1>(v.x*m.m00 + v.y*m.m10 + v.z*m.m20 + v.w*m.m30,
		v.x*m.m01 + v.y*m.m11 + v.z*m.m21 + v.w*m.m31,
		v.x*m.m02 + v.y*m.m12 + v.z*m.m22 + v.w*m.m32,
		v.x*m.m03 + v.y*m.m13 + v.z*m.m23 + v.w*m.m33);
}

template<class F>
inline std::ostream& operator<<(std::ostream& os, const TMatrix4x4<F>& A)
{
	os << "[" << A.m00 << ", " << A.m01 << ", " << A.m02 << "," << A.m03 << "\n"
	   << " " << A.m10 << ", " << A.m11 << ", " << A.m12 << "," << A.m13 << "\n"
	   << " " << A.m20 << ", " << A.m21 << ", " << A.m22 << "," << A.m23 << "\n"
	   << " " << A.m30 << ", " << A.m31 << ", " << A.m32 << "," << A.m33 << "]" << std::endl;
	return os;
}

template<class T>
inline TVector4<T> Vec3ToVec4(const TVector3<T>& v, T a = 1)
{
	return TVector4<T>(v.x, v.y, v.z, a);
}

template<class T>
inline TVector3<T> Vec4ToVec3(const TVector4<T>& v)
{
	return TVector3<T>(v.x, v.y, v.z);
}

template<class T>
inline TVector3<T> TransformPoint(const TVector3<T>& p, const TMatrix4x4<T>& m)
{
	return Vec4ToVec3(m * TVector4<T>(p, 1));
}

template<class T>
inline TVector3<T> TransformPointW(const TVector3<T>& p, const TMatrix4x4<T>& m)
{
	Vector4 v = m * TVector4<T>(p, 1);
	float invW = 1.0f / v.w;
	return Vector3(v.x * invW, v.y * invW, v.z * invW);
}

template<class T>
inline TVector3<T> TransformDirection(const TVector3<T>& v, const TMatrix4x4<T>& m)
{
	return Vec4ToVec3(m * TVector4<T>(v, 0));
}
