#pragma once

#include "MathCommon.h"

template<typename T>
class TVector4
{
public:
	const static int DIMENSION = 4;
	T x, y, z, w;

	TVector4() :x(0), y(0), z(0), w(0) {}

	TVector4& operator=(const TVector4& t)
	{
		x = t.x;
		y = t.y;
		z = t.z;
		w = t.w;
		return *this;
	}

	TVector4(T x, T y, T z, T w) :x(x), y(y), z(z), w(w) {}

	TVector4(T x, const TVector3<T>& t)
	{
		this->x = x;
		this->y = t.x;
		this->z = t.y;
		this->w = t.z;
	}

	TVector4(const TVector3<T>& t, T w)
	{
		this->x = t.x;
		this->y = t.y;
		this->z = t.z;
		this->w = w;
	}


	bool operator==(const TVector4& t) const {
		return x == t.x && y == t.y && z == t.z && w == t.w;
	}

	bool operator !=(const TVector4 &a) const {
		return x != a.x || y != a.y || z != a.z || w != a.w;
	}

	TVector4 operator -() const {
		return TVector4(-x, -y, -z, -w);
	}

	TVector4 operator +(T k) const {
		return TVector4(x + k, y + k, z + k, w + k);
	}
	TVector4 operator +(const TVector4 &a) const {
		return TVector4(x + a.x, y + a.y, z + a.z, w + a.w);
	}

	TVector4 operator -(T k) const {
		return TVector4(x - k, y - k, z - k, w - k);
	}
	TVector4 operator -(const TVector4 &a) const {
		return TVector4(x - a.x, y - a.y, z - a.z, w - a.w);
	}

	TVector4 operator *(T k) const {
		return TVector4(x*k, y*k, z*k, w*k);
	}

	TVector4 operator *(const TVector4 &a) const {
		return TVector4(x * a.x, y * a.y, z * a.z, w * a.w);
	}

	TVector4 operator /(T a) const {
		auto oneOverA = recip_tpl(a); // NOTE: no check for divide by zero here
		return TVector4<T>(x*oneOverA, y*oneOverA, z*oneOverA, w*oneOverA);
	}

	TVector4& operator +=(const TVector4 &a) {
		x += a.x; y += a.y; z += a.z; w += a.w;
		return *this;
	}

	TVector4& operator -=(const TVector4 &a) {
		x -= a.x; y -= a.y; z -= a.z; w -= a.w;
		return *this;
	}

	TVector4& operator *=(T a) {
		x *= a; y *= a; z *= a; w *= a;
		return *this;
	}

	TVector4& operator /=(T a) {
		auto oneOverA = recip_tpl(a);
		x *= oneOverA;
		y *= oneOverA;
		z *= oneOverA;
		w *= oneOverA;
		return *this;
	}

	void Normalize() {
		T magSq = x * x + y * y + z * z + w * w;
		if (magSq > 0) 
		{
			// check for divide-by-zero
			T oneOverMag = recip_tpl(sqrt_tpl(magSq));
			x *= oneOverMag;
			y *= oneOverMag;
			z *= oneOverMag;
			w *= oneOverMag;
		}
	}

	TVector4 GetNormalized() {
		TVector4<T> v(*this);
		v.Normalize();
		return v;
	}

	auto Length() const {
		return sqrt_tpl(x * x + y * y + z * z + w * w);
	}

	T LengthSquared() const {
		return x * x + y * y + z * z + w * w;
	}

	T Dot(const TVector4& a) const {
		return x * a.x + y * a.y + z * a.z + w * a.w;
	}

	auto Distance(const TVector4 &b) const {
		T dx = x - b.x;
		T dy = y - b.y;
		T dz = z - b.z;
		T dw = w - b.w;
		return sqrt_tpl(dx*dx + dy * dy + dz * dz + dw * dw);
	}

	T DistanceSquared(const TVector4& b) const {
		T dx = x - b.x;
		T dy = y - b.y;
		T dz = z - b.z;
		T dw = w - b.w;
		return (dx*dx + dy * dy + dz * dz + dw * dw);
	}

	T DistanceSquared2D(const TVector4 &b) const {
		T dx = x - b.x;
		T dy = y - b.y;
		return (dx * dx + dy * dy);
	}

	auto Distance2D(const TVector4 &b) const {
		return sqrt_tpl(DistanceSquared2D(b));
	}

	T DistanceSquared3D(const TVector4& b) const {
		T dx = x - b.x;
		T dy = y - b.y;
		T dz = z - b.z;
		return (dx*dx + dy * dy + dz * dz);
	}

	auto Distance3D(const TVector4 &b) const {
		return sqrt_tpl(DistanceSquared3D(b));
	}

};

template<class T>
inline T Dot(const TVector4<T> &a, const TVector4<T> &b)
{
	return a.dot(b);
}

template<>
inline bool TVector4<float>::operator==(const TVector4<float>& t) const {
	return fcmp_tpl(x, t.x) && fcmp_tpl(y, t.y) &&
		fcmp_tpl(z, t.z) && fcmp_tpl(w, t.w);
}

template<>
inline bool TVector4<double>::operator==(const TVector4<double>& t) const {
	return fcmp_tpl(x, t.x) && fcmp_tpl(y, t.y) &&
		fcmp_tpl(z, t.z) && fcmp_tpl(w, t.w);
}

template<>
inline bool TVector4<float>::operator!=(const TVector4<float>& t) const {
	return !fcmp_tpl(x, t.x) || !fcmp_tpl(y, t.y) ||
		!fcmp_tpl(z, t.z) || !fcmp_tpl(w, t.w);
}

template<>
inline bool TVector4<double>::operator!=(const TVector4<double>& t) const {
	return !fcmp_tpl(x, t.x) ||
		!fcmp_tpl(y, t.y) ||
		!fcmp_tpl(z, t.z) ||
		!fcmp_tpl(w, t.w);
}


using Vector4 = TVector4<float>;
using Vector4f = TVector4<float>;
using Vector4i = TVector4<int>;
using Vector4d = TVector4<double>;