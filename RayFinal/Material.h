#pragma once
#include "BSDF.h"
struct DifferentialGeometry;

// TODO: only supports lambertian now.
class BSDF
{
public:
	BSDF();
	void SampleBSDF(const Vector3& wo, Vector3* wi, float* pdf);
	Color f(const Vector3& wo, const Vector3& wi);
	void AddBxDF(BxDF* bxdf);
private:
	enum { MAX_BXDF_COUNT = 8 };
	BxDF*	mBxDFs[MAX_BXDF_COUNT];
	int		mSize;
	bool	mDelta;
};

class Material
{
public:
	//virtual BSDF* GetBSDF(const DifferentialGeometry& dg) = 0;
	virtual BxDF* GetBRDF(const DifferentialGeometry& dg) { return nullptr; }
	virtual BxDF* GetBTDF(const DifferentialGeometry& dg) { return nullptr; }
	virtual Color GetEmission() const { return Color(0, 0, 0, 0); }
};

class LambertianMaterial : public Material
{
public:
	LambertianMaterial(const Color& diffuseColor);
	//virtual BSDF* GetBSDF(const DifferentialGeometry& dg) override;
	virtual BxDF* GetBRDF(const DifferentialGeometry& dg) { return mBrdf; }
private:
	Color	mDiffuseColor;
	BxDF*	mBrdf;
};

class PureColorMaterial : public Material
{
public:
	PureColorMaterial(const Color& emission): mEmission(emission){}
	virtual Color GetEmission() const { return mEmission; }
	void SetEmission(const Color& emission) { mEmission = emission; }
	//virtual BSDF* GetBSDF(const DifferentialGeometry& dg) override;

private:
	Color	mEmission;
};

class SpecularDielectricMaterial : public Material
{
public:
	SpecularDielectricMaterial(const Color& R, const Color& T, float etai, float etat);
	virtual BxDF* GetBRDF(const DifferentialGeometry& dg) override { return mBrdf; }
	virtual BxDF* GetBTDF(const DifferentialGeometry& dg) override { return mBtdf; }
	~SpecularDielectricMaterial();
private:
	BxDF*		mBrdf;
	BxDF*		mBtdf;
	Fresnel*	mFresnel;
};

class SpecularConductorMaterial : public Material
{
public:
	SpecularConductorMaterial(const Color& R, float eta, float k);
	virtual BxDF* GetBRDF(const DifferentialGeometry& dg) override { return mBrdf; }
	~SpecularConductorMaterial();
private:
	BxDF*		mBrdf;
	Fresnel*	mFresnel;
};

class MicrofacetMaterial : public Material
{
public:
	MicrofacetMaterial(const Color& R, float eta, float k, float e);
	virtual ~MicrofacetMaterial();
	virtual BxDF* GetBRDF(const DifferentialGeometry& dg) override { return mBrdf; }
private:
	BxDF*						mBrdf;
	Fresnel*					mFresnel;
	MicrofacetDistribution*		mDistribution;
};
