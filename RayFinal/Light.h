#pragma once

#include "MathLib.h"
#include "Color.h"
#include "Sampler.h"


class ILight
{
public:
	virtual bool IsDelta() const = 0;
	virtual Color Sample_L(const Vector3& p, Vector3* wi, float* dist, float* pdf) = 0;
};

class HemisphereLight : public ILight
{
public:
	HemisphereLight(const Color& radiance, float fLightRange)
		:mRadiance(radiance), mLightRange(fLightRange)
	{

	}
	virtual bool IsDelta() const { return false; }
	virtual Color Sample_L(const Vector3& p, Vector3* wi, float* dist, float* pdf) override;

private:
	Color						mRadiance;
	UniformHemisphereSampler	mSampler;
	float						mLightRange;
};

class PlaneDiffuseAreaLight : public ILight
{
public:
	PlaneDiffuseAreaLight(const Matrix4x4& world, const Vector2& dim, const Color& radiance);
	virtual bool IsDelta() const { return false; }
	virtual Color Sample_L(const Vector3& p, Vector3* wi, float* dist, float* pdf) override;

private:
	Color						mRadiance;
	Vector2						mDimensions;
	Matrix4x4					mObjectToWorld;
	Vector3						mNs;
	float						mArea;
};

class DiskDiffuseAreaLight : public ILight
{
public:
	DiskDiffuseAreaLight(const Matrix4x4& worldTransform, float radius, const Color& radiance);
	virtual bool IsDelta() const { return false; }
	virtual Color Sample_L(const Vector3& p, Vector3* wi, float* dist, float* pdf) override;
private:
	Color						mRadiance;
	float						mRadius;
	Matrix4x4					mObjectToWorld;
	Vector3						mNs;
	float						mArea;
	DiskSampler					mSampler;
};

class PointLight : public ILight
{
public:
	PointLight(const Vector3 pos, const Color& intensity)
		:mPosition(pos), mIntensity(intensity) {}
	virtual bool IsDelta() const { return true; }
	virtual Color Sample_L(const Vector3& p, Vector3* wi, float* dist, float* pdf) override;
private:
	Color						mIntensity;
	Vector3						mPosition;
};



