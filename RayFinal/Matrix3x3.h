#pragma once
#include "Vector3.h"

template<class F>
class TMatrix3x3 {

public:

	TMatrix3x3(void) 
	{

	}

	TMatrix3x3(F * data)
	{
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				(*this)(i, j) = data[i * 3 + j];
			}
		}
	}

	explicit TMatrix3x3(F x00, F x01, F x02, F x10, F x11, F x12, F x20, F x21, F x22)
	{
		m00 = x00; m01 = x01; m02 = x02;
		m10 = x10; m11 = x11; m12 = x12;
		m20 = x20; m21 = x21; m22 = x22;
	}
	template<class F1, class F2, class F3, 
		class F4, class F5, class F6,
		class F7, class F8, class F9>
	explicit TMatrix3x3(F1 x00, F2 x01, F3 x02, F4 x10, F5 x11, F6 x12, F7 x20, F8 x21, F9 x22)
	{
		m00 = F(x00); m01 = F(x01); m02 = F(x02);
		m10 = F(x10); m11 = F(x11); m12 = F(x12);
		m20 = F(x20); m21 = F(x21); m22 = F(x22);
	}

	inline void Transpose() { // in-place transposition
		F t;
		t = m01; m01 = m10; m10 = t;
		t = m02; m02 = m20; m20 = t;
		t = m12; m12 = m21; m21 = t;
	}
	inline auto GetTransposed() const {
		TMatrix3x3<F> dst;
		dst.m00 = m00;			dst.m01 = m10;			dst.m02 = m20;
		dst.m10 = m01;			dst.m11 = m11;			dst.m12 = m21;
		dst.m20 = m02;			dst.m21 = m12;			dst.m22 = m22;
		return dst;
	}

	inline auto T() const { return GetTransposed(); }

	inline bool Invert(void)
	{
		TMatrix3x3<F>	m = *this;

		m00 = m.m22*m.m11 - m.m12*m.m21;	m01 = m.m02*m.m21 - m.m22*m.m01;	m02 = m.m12*m.m01 - m.m02*m.m11;
		m10 = m.m12*m.m20 - m.m22*m.m10;	m11 = m.m22*m.m00 - m.m02*m.m20;	m12 = m.m02*m.m10 - m.m12*m.m00;
		m20 = m.m10*m.m21 - m.m20*m.m11;	m21 = m.m20*m.m01 - m.m00*m.m21;	m22 = m.m00*m.m11 - m.m10*m.m01;

		F det = (m.m00*m00 + m.m10*m01 + m.m20*m02);
		if (fabs_tpl(det) < 1E-20f)
			return 0;

		F idet = (F)1.0 / det;
		m00 *= idet; m01 *= idet;	m02 *= idet;
		m10 *= idet; m11 *= idet;	m12 *= idet;
		m20 *= idet; m21 *= idet;	m22 *= idet;
		return 1;
	}

	inline auto GetInverted() const
	{
		TMatrix3x3<F> dst = *this; dst.Invert(); return dst;
	}

	inline float Determinant() const 
	{
		return (m00*m11*m22) + (m01*m12*m20) + (m02*m10*m21)
			- (m02*m11*m20) - (m00*m12*m21) - (m01*m10*m22);
	}

	F& operator()(int i, int j)
	{
		return m_entries[i][j];
	}

	F operator()(int i, int j) const
	{
		return m_entries[i][j];
	}

	inline static TMatrix3x3<F> Identity()
	{
		static TMatrix3x3<F> t(1, 0, 0, 0, 1, 0, 0, 0, 1);
		return t;
	}
	inline static TMatrix3x3<F> Zero() 
	{
		static TMatrix3x3<F> t(0, 0, 0, 0, 0, 0, 0, 0, 0);
		return t;
	}

public:
	union
	{
		struct {
			F m00, m01, m02;
			F m10, m11, m12;
			F m20, m21, m22;
		};

		F m_entries[3][3];
	};
	// column vectors
	

}; // class Matrix3x3

using Matrix3x3 = TMatrix3x3<float>;

template<class F1, class F2>
inline TMatrix3x3<F1> operator*(const TMatrix3x3<F1>& l, const TMatrix3x3<F2>& r)
{
	TMatrix3x3<F1> m;
	m.m00 = l.m00*r.m00 + l.m01*r.m10 + l.m02*r.m20;
	m.m01 = l.m00*r.m01 + l.m01*r.m11 + l.m02*r.m21;
	m.m02 = l.m00*r.m02 + l.m01*r.m12 + l.m02*r.m22;
	m.m10 = l.m10*r.m00 + l.m11*r.m10 + l.m12*r.m20;
	m.m11 = l.m10*r.m01 + l.m11*r.m11 + l.m12*r.m21;
	m.m12 = l.m10*r.m02 + l.m11*r.m12 + l.m12*r.m22;
	m.m20 = l.m20*r.m00 + l.m21*r.m10 + l.m22*r.m20;
	m.m21 = l.m20*r.m01 + l.m21*r.m11 + l.m22*r.m21;
	m.m22 = l.m20*r.m02 + l.m21*r.m12 + l.m22*r.m22;
	return m;
}

template<class F1, class F2>
inline TMatrix3x3<F1> operator*(const TMatrix3x3<F1>& m, F2 op)
{
	TMatrix3x3<F1> res;
	res.m00 = m.m00*op; res.m01 = m.m01*op;	res.m02 = m.m02*op;
	res.m10 = m.m10*op; res.m11 = m.m11*op; res.m12 = m.m12*op;
	res.m20 = m.m20*op; res.m21 = m.m21*op;	res.m22 = m.m22*op;
	return res;
}

template<class F1, class F2>
inline TMatrix3x3<F1> operator*(const TMatrix3x3<F1>& m, const TVector3<F2>& p)
{
	TVector3<F1> tp;
	tp.x = F1(m.m00*p.x + m.m01*p.y + m.m02*p.z);
	tp.y = F1(m.m10*p.x + m.m11*p.y + m.m12*p.z);
	tp.z = F1(m.m20*p.x + m.m21*p.y + m.m22*p.z);
	return	tp;
}

template<class F1, class F2>
inline TVector3<F1> operator*(const TMatrix3x3<F1>& m, const TVector3<F2>& p)
{
	TVector3<F1> tp;
	tp.x = p.x*m.m00 + p.y*m.m10 + p.z*m.m20;
	tp.y = p.x*m.m01 + p.y*m.m11 + p.z*m.m21;
	tp.z = p.x*m.m02 + p.y*m.m12 + p.z*m.m22;
	return	tp;
}

template<class F1, class F2>
inline TMatrix3x3<F1> operator+(const TMatrix3x3<F1>& l, const TMatrix3x3<F2>& r)
{
	TMatrix3x3<F1> res;
	res.m00 = l.m00 + r.m00;  res.m01 = l.m01 + r.m01;	res.m02 = l.m02 + r.m02;
	res.m10 = l.m10 + r.m10;  res.m11 = l.m11 + r.m11;  res.m12 = l.m12 + r.m12;
	res.m20 = l.m20 + r.m20;  res.m21 = l.m21 + r.m21;  res.m22 = l.m22 + r.m22;
	return res;
}

template<class F1, class F2>
inline TMatrix3x3<F1>& operator+=(TMatrix3x3<F1>& l, const TMatrix3x3<F2>& r)
{
	l.m00 += r.m00; l.m01 += r.m01;	l.m02 += r.m02;
	l.m10 += r.m10;	l.m11 += r.m11; l.m12 += r.m12;
	l.m20 += r.m20; l.m21 += r.m21;	l.m22 += r.m22;
	return l;
}

template<class F1, class F2>
inline TMatrix3x3<F1> operator-(const TMatrix3x3<F1>& l, const TMatrix3x3<F2>& r)
{
	TMatrix3x3<F1> res;
	res.m00 = l.m00 - r.m00;	res.m01 = l.m01 - r.m01;	res.m02 = l.m02 - r.m02;
	res.m10 = l.m10 - r.m10; 	res.m11 = l.m11 - r.m11; 	res.m12 = l.m12 - r.m12;
	res.m20 = l.m20 - r.m20; 	res.m21 = l.m21 - r.m21; 	res.m22 = l.m22 - r.m22;
	return res;
}

template<class F1, class F2>
inline TMatrix3x3<F1>& operator-=(TMatrix3x3<F1>& l, const TMatrix3x3<F2>& r)
{
	l.m00 -= r.m00; l.m01 -= r.m01;	l.m02 -= r.m02;
	l.m10 -= r.m10;	l.m11 -= r.m11; l.m12 -= r.m12;
	l.m20 -= r.m20; l.m21 -= r.m21;	l.m22 -= r.m22;
	return l;
}


template<class F>
inline std::ostream& operator<<(std::ostream& os, const TMatrix3x3<F>& A)
{
	os << "[" << A.m00 << ", " << A.m01 << ", " << A.m02 << "\n"
	   << " " << A.m10 << ", " << A.m11 << ", " << A.m12 << "\n"
	   << " " << A.m20 << ", " << A.m21 << ", " << A.m22 << "]" << std::endl;
	return os;
}
