#pragma once
#include "MathLib.h"

class MicrofacetDistribution
{
public:
	virtual float D(const Vector3& wh) const = 0;
	virtual float G(const Vector3& wi, const Vector3& wo, const Vector3& wh) const;
	virtual void Sample_D(const Vector3& wo, Vector3* wi, float* pdf) const = 0;
};

class BlinnDistribution : public MicrofacetDistribution
{
public:
	BlinnDistribution(float e):mExponent(e){}
	virtual float D(const Vector3& wh) const override;
	virtual void Sample_D(const Vector3& wo, Vector3* wi, float* pdf) const override;
private:
	float mExponent;
};

