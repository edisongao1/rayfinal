#include "pch.h"
#include "SceneLoader.h"
#include "Scene.h"
#include "Transform.h"
#include "MeshManager.h"
#include "MeshEntity.h"
#include "RenderWindow.h"
#include "Light.h"
#include "Material.h"
#include "MicrofacetDistribution.h"
#include "Fresnel.h"

SceneLoader::SceneLoader(RenderWindow* window)
	:mWindow(window)
{

}


SceneLoader::~SceneLoader()
{
	
}

int SceneLoader::GetFloatArrayFromString(const char* str, float arr[], int maxNum)
{
	if (!str)
		return 0;

	char buff[128] = { 0 };
	const char* p = str;
	int i = 0, j = 0;
	while (*p != '\0') {
		if ((*p >= '0' && *p <= '9') || *p == '.' || *p == '-') {
			buff[i++] = *p;
		}
		else if (*p == ',') {
			buff[i] = '\0';
			arr[j++] = (float)atof(buff);
			i = 0;
			if (j == maxNum)
				return maxNum;
		}
		p += 1;
	}

	if (i != 0) {
		buff[i] = '\0';
		arr[j++] = (float)atof(buff);
	}
	return j;
}

bool SceneLoader::GetVectorFromString(const char* str, Vector3& v)
{
	float arr[3] = { 0 };
	if (GetFloatArrayFromString(str, arr, 3) != 3) {
		return false;
	}
	v.x = arr[0];
	v.y = arr[1];
	v.z = arr[2];
	return true;
}

bool SceneLoader::GetVectorFromString(const char* str, Vector2& v)
{
	float arr[2] = { 0 };
	if (GetFloatArrayFromString(str, arr, 2) != 3) {
		return false;
	}
	v.x = arr[0];
	v.y = arr[1];
	return true;
}

void SceneLoader::GetColorFromString(const char* str, Color& color)
{
	float arr[4] = { 0 };
	GetFloatArrayFromString(str, arr, 4);
	color.r = arr[0];
	color.g = arr[1];
	color.b = arr[2];
	color.a = arr[3];
}

Scene* SceneLoader::Load(const char* fileName)
{
	tinyxml2::XMLDocument doc;
	if (doc.LoadFile(fileName) != XML_SUCCESS)
		return nullptr;

	Scene* scene = new Scene();
	XMLElement* root = doc.RootElement();

	XMLElement* CameraNode = root->FirstChildElement("Camera");
	if (CameraNode) {
		ParseCamera(scene, CameraNode);
	}

	XMLElement* Materials = root->FirstChildElement("Materials");
	if (Materials)
	{
		XMLElement* MaterialNode = Materials->FirstChildElement("Material");
		while (MaterialNode) {
			ParseMaterial(MaterialNode);
			MaterialNode = MaterialNode->NextSiblingElement("Material");
		}
	}

	XMLElement* Entities = root->FirstChildElement("Entities");
	if (Entities)
	{
		XMLElement* EntityNode = Entities->FirstChildElement("Entity");
		while (EntityNode) {
			ParseEntity(scene, EntityNode);
			EntityNode = EntityNode->NextSiblingElement("Entity");
		}
	}

	XMLElement* Lights = root->FirstChildElement("Lights");
	if (Lights)
	{
		XMLElement* LightNode = Lights->FirstChildElement("Light");
		while (LightNode) {
			ParseLight(scene, LightNode);
			LightNode = LightNode->NextSiblingElement("Light");
		}
	}
	
	return scene;
}

void SceneLoader::ParseCamera(Scene* scene, XMLElement* xmlNode)
{
	Vector3 position(0, 0, 1);
	Vector3 direction(0, 0, -1);
	float fov = 50.0f;
	float fFarZ = 0.1f, fNearZ = 1000.f;

	XMLElement* PositionNode = xmlNode->FirstChildElement("Position");
	if (PositionNode) {
		GetVectorFromString(PositionNode->GetText(), position);
	}

	XMLElement* DirectionNode = xmlNode->FirstChildElement("Direction");
	if (PositionNode) {
		GetVectorFromString(DirectionNode->GetText(), direction);
	}

	GetFloatFromChildText(xmlNode, "Fov", &fov);
	GetFloatFromChildText(xmlNode, "NearZ", &fNearZ);
	GetFloatFromChildText(xmlNode, "FarZ", &fFarZ);

	direction.Normalize();

	scene->mCamera.Init(position, direction, mWindow->GetSize(),
		radians(fov), fNearZ, fFarZ);
}

void SceneLoader::ParseEntity(Scene* scene, XMLElement* xmlNode)
{
	const char* szEntityType = nullptr;
	const char* id = nullptr;
	if (xmlNode->QueryStringAttribute("type", &szEntityType) != XML_SUCCESS)
	{
		return;
	}

	if (xmlNode->QueryStringAttribute("id", &id) != XML_SUCCESS)
	{
		return;
	}

	ISceneEntity* pSceneEntity = nullptr;
	if (_stricmp(szEntityType, "plane") == 0)
	{
		pSceneEntity = ParsePlaneEntity(scene, id, xmlNode);
	}
	else if (_stricmp(szEntityType, "sphere") == 0)
	{
		pSceneEntity = ParseSphereEntity(scene, id, xmlNode);
	}
	else if (_stricmp(szEntityType, "disk") == 0)
	{
		pSceneEntity = ParseDiskEntity(scene, id, xmlNode);
	}

	if (pSceneEntity) {
		XMLElement* MaterialNode = xmlNode->FirstChildElement("Material");
		if (MaterialNode) {
			const char* szMaterialName = MaterialNode->GetText();
			auto it = mMaterialMap.find(szMaterialName);
			if (it != mMaterialMap.end()) {
				pSceneEntity->SetMaterial(it->second);
			}
		}
	}
}


void SceneLoader::ParseLight(Scene* scene, XMLElement* xmlNode)
{
	const char* szLightType = nullptr;
	const char* id = nullptr;
	if (xmlNode->QueryStringAttribute("type", &szLightType) != XML_SUCCESS) {
		return;
	}

	if (xmlNode->QueryStringAttribute("id", &id) != XML_SUCCESS) {
		return;
	}

	ILight* pLight = nullptr;

	Matrix4x4 worldTransform;
	XMLElement* TranslateNode = xmlNode->FirstChildElement("Transform");
	if (TranslateNode) {
		ParseTransform(TranslateNode, worldTransform);
	}
	Color radiance;
	GetColorFromChildText(xmlNode, "Radiance", radiance);

	if (_stricmp(szLightType, "Hemisphere") == 0) {
		float range;
		GetFloatFromChildText(xmlNode, "Range", &range);
		pLight = new HemisphereLight(radiance, range);
	}
	else if (_stricmp(szLightType, "PlaneArea") == 0) {
		float fSize[2] = { 1, 1 };
		XMLElement* SizeNode = xmlNode->FirstChildElement("Size");
		if (SizeNode) {
			GetFloatArrayFromString(SizeNode->GetText(), fSize, 2);
		}
		Vector2 dim(fSize[0], fSize[1]);
		pLight = new PlaneDiffuseAreaLight(worldTransform, dim, radiance);
	}
	else if (_stricmp(szLightType, "DiskArea") == 0) {
		float radius;
		GetFloatFromChildText(xmlNode, "Radius", &radius);
		pLight = new DiskDiffuseAreaLight(worldTransform, radius, radiance);
	}
	else if (_stricmp(szLightType, "Point") == 0) {
		Vector3 position;
		Color intensity;
		GetVectorFromChildText(xmlNode, "Position", position);
		GetColorFromChildText(xmlNode, "Intensity", intensity);
		pLight = new PointLight(position, intensity);
	}
	if (pLight != nullptr) {
		scene->AddLight(pLight);
	}
}

void SceneLoader::ParseMaterial(XMLElement* xmlNode)
{
	const char* szType = nullptr;
	const char* szName = nullptr;
	if (xmlNode->QueryStringAttribute("type", &szType) != XML_SUCCESS) {
		return;
	}
	if (xmlNode->QueryStringAttribute("name", &szName) != XML_SUCCESS) {
		return;
	}
	auto it = mMaterialMap.find(szName);
	if (it != mMaterialMap.end())
		return;

	Material* pMaterial = nullptr;
	if (_stricmp(szType, "lambertian") == 0) {
		Color diffuseColor;
		GetColorFromChildText(xmlNode, "Diffuse", diffuseColor);
		pMaterial = new LambertianMaterial(diffuseColor);
	}
	else if (_stricmp(szType, "pure-color") == 0) {
		Color emissionColor;
		GetColorFromChildText(xmlNode, "Emission", emissionColor);
		pMaterial = new PureColorMaterial(emissionColor);
	}
	else if (_stricmp(szType, "specularConductor") == 0) {
		Color R;
		float eta = 1.0, k = 1.0;
		GetColorFromChildText(xmlNode, "Reflection", R);
		GetFloatFromChildText(xmlNode, "RefractionIndex", &eta);
		GetFloatFromChildText(xmlNode, "AbsorptionCoefficient", &k);
		pMaterial = new SpecularConductorMaterial(R, eta, k);
	}
	else if (_stricmp(szType, "specularDielectric") == 0) {
		Color R, T;
		float eta;
		GetColorFromChildText(xmlNode, "Reflection", R);
		GetColorFromChildText(xmlNode, "Transmission", T);
		GetFloatFromChildText(xmlNode, "RefractionIndex", &eta);
		pMaterial = new SpecularDielectricMaterial(R, T, 1.0f, eta);
	}
	else if (_stricmp(szType, "TorranceSparrow") == 0) {
		Color R;
		float eta = 1.0, k = 1.0, e = 1.0f;
		GetColorFromChildText(xmlNode, "Reflection", R);
		GetFloatFromChildText(xmlNode, "RefractionIndex", &eta);
		GetFloatFromChildText(xmlNode, "AbsorptionCoefficient", &k);
		GetFloatFromChildText(xmlNode, "BlinnExponent", &e);
		pMaterial = new MicrofacetMaterial(R, eta, k, e);
	}
	
	if (pMaterial)
	{
		mMaterialMap.insert(std::make_pair(szName, pMaterial));
	}
}

ISceneEntity* SceneLoader::ParsePlaneEntity(Scene* scene, const char* id, XMLElement* xmlNode)
{
	Matrix4x4 worldTransform;
	XMLElement* TranslateNode = xmlNode->FirstChildElement("Transform");
	if (TranslateNode) {
		ParseTransform(TranslateNode, worldTransform);
	}

	float fSize[] = { 1, 1 };
	XMLElement* ParamsNode = xmlNode->FirstChildElement("Params");
	if (ParamsNode)
	{
		XMLElement* SizeNode = ParamsNode->FirstChildElement("Size");
		GetFloatArrayFromString(SizeNode->GetText(), fSize, 2);
	}
	
	Vector2 size(fSize[0], fSize[1]);
	TriangleMesh* pMesh = MeshManager::Get().CreatePlaneMesh(std::string(id), size);
	if (!pMesh)
		return 0;

	MeshEntity* pMeshEntity = new MeshEntity(pMesh, worldTransform);
	scene->AddEntity(pMeshEntity);
	return pMeshEntity;
}

ISceneEntity* SceneLoader::ParseSphereEntity(Scene* scene, const char* id, XMLElement* xmlNode)
{
	Matrix4x4 worldTransform;
	XMLElement* TranslateNode = xmlNode->FirstChildElement("Transform");
	if (TranslateNode) {
		ParseTransform(TranslateNode, worldTransform);
	}

	float fRadius = 1.0f;
	XMLElement* ParamsNode = xmlNode->FirstChildElement("Params");
	if (ParamsNode)
	{
		GetFloatFromChildText(ParamsNode, "Radius", &fRadius);
	}
	
	Sphere* sphereEntity = new Sphere(fRadius, worldTransform);
	scene->AddEntity(sphereEntity);
	return sphereEntity;
}

ISceneEntity* SceneLoader::ParseDiskEntity(Scene* scene, const char* id, XMLElement* xmlNode)
{
	Matrix4x4 worldTransform;
	XMLElement* TranslateNode = xmlNode->FirstChildElement("Transform");
	if (TranslateNode) {
		ParseTransform(TranslateNode, worldTransform);
	}

	float fRadius = 1.0f;
	XMLElement* ParamsNode = xmlNode->FirstChildElement("Params");
	if (ParamsNode)
	{
		GetFloatFromChildText(ParamsNode, "Radius", &fRadius);
	}

	Disk* diskEntity = new Disk(fRadius, worldTransform);
	scene->AddEntity(diskEntity);
	return diskEntity;
}

void SceneLoader::ParseTransform(XMLElement* xmlNode, Matrix4x4& matrix)
{
	float translations[3] = { 1, 1, 1 };
	float rotations[3] = { 0, 0, 0 };
	float scalings[3] = { 1, 1, 1 };
	
	XMLElement* TranslateNode = xmlNode->FirstChildElement("Translate");
	if (TranslateNode) {
		GetFloatArrayFromString(TranslateNode->GetText(), translations, 3);
	}

	XMLElement* RotateNode = xmlNode->FirstChildElement("Rotate");
	if (RotateNode) {
		GetFloatArrayFromString(RotateNode->GetText(), rotations, 3);
	}

	XMLElement* ScaleNode = xmlNode->FirstChildElement("Scale");
	if (ScaleNode) {
		if (GetFloatArrayFromString(ScaleNode->GetText(), scalings, 3) == 1) {
			scalings[1] = scalings[2] = scalings[0];
		}
	}

	matrix = CreateTranslation(translations[0], translations[1], translations[2])
		* CreateRotation(radians(rotations[0]), radians(rotations[1]), radians(rotations[2]))
		* CreateScaling(scalings[0], scalings[1], scalings[2]);
}

bool SceneLoader::GetFloatFromChildText(XMLElement* xmlNode, const char* childTagName, float* val)
{
	XMLElement* childNode = xmlNode->FirstChildElement(childTagName);
	if (!childNode)
		return false;
	*val = (float)atof(childNode->GetText());
	return true;
}

bool SceneLoader::GetColorFromChildText(XMLElement* xmlNode, const char* childTagName, Color& color)
{
	XMLElement* childNode = xmlNode->FirstChildElement(childTagName);
	if (!childNode)
		return false;
	GetColorFromString(childNode->GetText(), color);
	return true;
}

bool SceneLoader::GetVectorFromChildText(XMLElement* xmlNode, const char* childTagName, Vector3& v)
{
	XMLElement* childNode = xmlNode->FirstChildElement(childTagName);
	if (!childNode)
		return false;
	GetVectorFromString(childNode->GetText(), v);
	return true;
}

bool SceneLoader::GetVectorFromChildText(XMLElement* xmlNode, const char* childTagName, Vector2& v)
{
	XMLElement* childNode = xmlNode->FirstChildElement(childTagName);
	if (!childNode)
		return false;
	GetVectorFromString(childNode->GetText(), v);
	return true;
}

