#include "pch.h"
#include "Shape.h"
#include "Scene.h"

bool Sphere::RayIntersect(const Ray& ray, DifferentialGeometry& dg)
{
	// transform ray to local coordination
	Ray r = ray.Transformed(mWorldToObject);

	float a = r.d.LengthSquared();
	float b = 2 * r.o.Dot(r.d);
	float c = r.o.LengthSquared() - mRadius * mRadius;

	float delta = b * b - 4 * a * c;
	if (delta < 0)
		return false;
	float sqrt_delta = sqrtf(delta);
	float t1 = (-b - sqrt_delta) / (2 * a);
	if (t1 > r.maxt)
		return false;

	if (t1 < r.mint)
	{
		float t2 = (-b + sqrt_delta) / (2 * a);
		if (t2 >= r.mint && t2 <= r.maxt)
		{
			t1 = t2;
		}
		else
		{
			return false;
		}
	}

	static const float epsilon = 0.000001f;
	Vector3 p = r(t1);
	float costheta = p.z / mRadius;
	float sintheta = sqrtf(1.0f - costheta * costheta);
	float cottheta = costheta / (sintheta == 0.0f ? epsilon : sintheta);

	float invZRadius = 1.0f / sqrtf(p.x * p.x + p.y * p.y);
	float cosphi = p.x * invZRadius;
	float sinphi = p.y * invZRadius;

	dg.t = t1;
	dg.pos = TransformPoint(p, mObjectToWorld);
	dg.ns = TransformDirection(p.GetNormalized(), mObjectToWorld);

	//dg.dpdu = Vector3(-_2PI * mRadius * sintheta * sinphi,
	//	_2PI * mRadius * sintheta * cosphi, 0);
	dg.dpdu = Vector3(-_2PI * p.y, _2PI * p.x, 0) * mRadius;
	dg.dpdv = Vector3(PI * p.z * cosphi, PI * p.z * sinphi, -PI * mRadius * sintheta);

	dg.dpdu = TransformDirection(dg.dpdu, mObjectToWorld);
	dg.dpdv = TransformDirection(dg.dpdv, mObjectToWorld);
	dg.ts = dg.dpdu.GetNormalized();
	dg.bs = dg.ns.Cross(dg.ts);

	//dg.u = acosf(costheta);
	return true;
}

bool Sphere::RayIntersectFast(const Ray& ray, float& t)
{
	// transform ray to local coordination
	Ray r = ray.Transformed(mWorldToObject);

	float a = r.d.LengthSquared();
	float b = 2 * r.o.Dot(r.d);
	float c = r.o.LengthSquared() - mRadius * mRadius;

	float delta = b * b - 4 * a * c;
	if (delta < 0)
		return false;
	float sqrt_delta = sqrtf(delta);
	float t1 = (-b - sqrt_delta) / (2 * a);
	if (t1 > r.maxt)
		return false;

	if (t1 < r.mint)
	{
		float t2 = (-b + sqrt_delta) / (2 * a);
		if (t2 >= r.mint && t2 <= r.maxt)
		{
			t1 = t2;
		}
		else
		{
			return false;
		}
	}

	t = t1;
	return true;
}

void Sphere::AddToScene(Scene* scene)
{
	scene->mObjects.push_back(this);
}

bool Disk::RayIntersect(const Ray& ray, DifferentialGeometry& dg)
{
	Ray r = ray.Transformed(mWorldToObject);
	if (r.d.z == 0.0f)
		return false;

	float t = -r.o.z / r.d.z;
	if (t < ray.mint || t > ray.maxt)
		return false;

	Vector3 localPos = r(t);
	
	float radius2 = localPos.LengthSquared();
	if (radius2 > mRadius * mRadius)
		return false;

	float radius = sqrtf(radius2);
	dg.pos = TransformPoint(localPos, mObjectToWorld);
	dg.t = t;
	
	float costheta = (radius == 0) ? 1.0f : localPos.x / radius;
	float sintheta = (radius == 0) ? 0.0f : localPos.y / radius;

	costheta = clamp_tpl(costheta, -1.0f, 1.0f);
	sintheta = clamp_tpl(sintheta, -1.0f, 1.0f);

	float theta = acosf(costheta);
	if (sintheta < 0) {
		theta = _2PI - theta;
	}

	dg.u = theta * INV_2PI;
	dg.v = radius / mRadius;

	dg.dpdu = Vector3(-_2PI * localPos.y, _2PI * localPos.x, 0);
	dg.dpdv = Vector3(mRadius * costheta, mRadius * sintheta, 0);

	dg.dpdu = TransformDirection(dg.dpdu, mObjectToWorld);
	dg.dpdv = TransformDirection(dg.dpdv, mObjectToWorld);

	dg.ns = Vector3(0, 0, 1.0f);
	dg.ts = Vector3(-sintheta, costheta, 0); // normalize dpdu
	dg.bs = Vector3(costheta, sintheta, 0); // normalizze dpdv

	return true;
}

bool Disk::RayIntersectFast(const Ray& ray, float& t)
{
	Ray r = ray.Transformed(mWorldToObject);
	if (r.d.z == 0.0f)
		return false;

	t = -r.o.z / r.d.z;
	if (t < ray.mint || t > ray.maxt)
		return false;

	Vector3 localPos = r(t);

	float radius2 = localPos.LengthSquared();
	if (radius2 > mRadius * mRadius)
		return false;

	return true;
}

void Disk::AddToScene(Scene* scene)
{
	scene->mObjects.push_back(this);
}
