#pragma once

#include "MathCommon.h"
#include "Matrix4x4.h"

class Ray
{
public:
	Vector3 o;
	Vector3 d;
	float mint, maxt;
	int depth;

	Ray():mint(0),maxt(FLT_MAX), depth(0) {}
	Ray(const Vector3& o, const Vector3& d, float mint = 0, float maxt = FLT_MAX)
		:o(o), d(d), mint(mint), maxt(maxt), depth(0)
	{

	}

	Vector3 operator()(float t) const {
		return o + d * t;
	}

	void Transform(const Matrix4x4& m) {
		o = TransformPoint(o, m);
		d = TransformDirection(d, m);
	}

	Ray Transformed(const Matrix4x4& m) const {
		Ray ray = *this;
		ray.Transform(m);
		return ray;
	}
};

struct RayIntersectResult
{
	Vector3 pos;
	Vector3 normal;
	float	t;

	void Transform(const Matrix4x4& m) 
	{
		pos = TransformPoint(pos, m);
		normal = TransformDirection(normal, m);
	}

	RayIntersectResult GetTransformed(const Matrix4x4& m) const
	{
		RayIntersectResult result;
		result.pos = TransformPoint(pos, m);
		result.normal = TransformDirection(normal, m);
		result.t = t;
	}
};




