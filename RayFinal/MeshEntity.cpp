#include "pch.h"
#include "MeshEntity.h"
#include "Scene.h"
#include "MeshManager.h"

void Triangle::Init(MeshEntity* pEntity, int* indices)
{
	this->mEntity = pEntity;
	this->mIndices = indices;

	// dpdu, dpdv
	// normal, tangent
	const Vector3& p0 = mEntity->mPositions[mIndices[0]];
	const Vector3& p1 = mEntity->mPositions[mIndices[1]];
	const Vector3& p2 = mEntity->mPositions[mIndices[2]];

	SUVCoordinate uv0 = mEntity->mUVs[mIndices[0]];
	SUVCoordinate uv1 = mEntity->mUVs[mIndices[1]];
	SUVCoordinate uv2 = mEntity->mUVs[mIndices[2]];

	float u0 = uv0.u; float v0 = uv0.v;
	float u1 = uv1.u; float v1 = uv1.v;
	float u2 = uv2.u; float v2 = uv2.v;

	float du1 = u1 - u0;
	float dv1 = v1 - v0;
	float du2 = u2 - u0;
	float dv2 = v2 - v0;

	float det = du1 * dv2 - du2 * dv1;
	float invDet = 1.0f / det;

	Vector3 e1 = p1 - p0;
	Vector3 e2 = p2 - p0;

	dpdu = (e1 * dv2 - e2 * dv1) * invDet;
	dpdv = (e2 * du1 - e1 * du2) * invDet;

	ns = dpdu.Cross(dpdv);
	ns.Normalize();

	ts = dpdu.GetNormalized();
	bs = ns.Cross(ts).GetNormalized();

	Vector3 c = p0 - dpdu * u0 - dpdv * v0;
	Vector3 p11 = dpdu * u1 + dpdv * v1 + c;
	Vector3 p22 = dpdu * u2 + dpdv * v2 + c;
	int a = 1;
}

bool Triangle::RayIntersect(const Ray& ray, DifferentialGeometry& dg)
{
	const Vector3& p0 = mEntity->mPositions[mIndices[0]];
	const Vector3& p1 = mEntity->mPositions[mIndices[1]];
	const Vector3& p2 = mEntity->mPositions[mIndices[2]];

	Vector3 e1 = p0 - p1;
	Vector3 e2 = p0 - p2;
	Vector3 e = p0 - ray.o;

	float invDet = 1.0f / (Cross(ray.d, e1).Dot(e2));
	float t = e.Cross(e1).Dot(e2) * invDet;

	if (t < ray.mint || t > ray.maxt)
		return false;

	float u = ray.d.Cross(e).Dot(e2) * invDet;

	if (u < 0 || u > 1.0f)
		return false;

	float v = ray.d.Cross(e1).Dot(e) * invDet;

	if (v < 0 || v > 1.0f)
		return false;

	if (v + u > 1.0f)
		return false;

	//result.t = t;
	//result.pos = p0 * (1.0f - u - v) + p1 * u + p2 * v;
	//result.normal = mEntity->mNormals[mIndices[0]] * (1.0f - u - v)
	//	+ mEntity->mNormals[mIndices[1]] * u
	//	+ mEntity->mNormals[mIndices[2]] * v;
	//result.normal = ns;

	dg.t = t;
	dg.pos = p0 * (1.0f - u - v) + p1 * u + p2 * v;
	dg.ns = ns;
	dg.ts = ts;
	dg.bs = bs;
	
	dg.dpdu = dpdu;
	dg.dpdv = dpdv;

	return true;
}

bool Triangle::RayIntersectFast(const Ray& ray, float& t)
{
	const Vector3& p0 = mEntity->mPositions[mIndices[0]];
	const Vector3& p1 = mEntity->mPositions[mIndices[1]];
	const Vector3& p2 = mEntity->mPositions[mIndices[2]];

	Vector3 e1 = p0 - p1;
	Vector3 e2 = p0 - p2;
	Vector3 e = p0 - ray.o;

	float invDet = 1.0f / (Cross(ray.d, e1).Dot(e2));
	t = e.Cross(e1).Dot(e2) * invDet;

	if (t < ray.mint || t > ray.maxt)
		return false;

	float u = ray.d.Cross(e).Dot(e2) * invDet;
	if (u < 0 || u > 1.0f)
		return false;

	float v = ray.d.Cross(e1).Dot(e) * invDet;
	if (v < 0 || v > 1.0f)
		return false;

	if (v + u > 1.0f)
		return false;

	return true;
}

ISceneEntity* Triangle::GetSceneEntity()
{
	return mEntity;
}

//MeshEntity::MeshEntity(const Vector3* positions, 
//	const Vector3* normals,
//	int vertexCount,
//	int* indices, 
//	int indiceCount,
//	const Matrix4x4& worldTransform)
//	: ISceneEntity(worldTransform)
//	, mVertexCount(vertexCount)
//	, mIndices(indices)
//	, mIndiceCount(indiceCount)
//{
//	mNormalObjectToWorld = mObjectToWorld.GetInverted().GetTransposed();
//	InitVertexBuffers(positions, normals);
//}

MeshEntity::MeshEntity(TriangleMesh* mesh, 
	const Matrix4x4& worldTransform)
	: ISceneEntity(worldTransform)
	, mVertexCount(mesh->mVertexCount)
	, mIndices(mesh->mIndices)
	, mIndiceCount(mesh->mIndiceCount)
	, mNormals(nullptr)
	, mUVs(mesh->mUVs)
{
	mNormalObjectToWorld = mObjectToWorld.GetInverted().GetTransposed();

	mPositions = new Vector3[mVertexCount];
	for (int i = 0; i < mVertexCount; i++) {
		mPositions[i] = TransformPoint(mesh->mPositions[i], mObjectToWorld);
	}
	
	if (mesh->mNormals) {
		mNormals = new Vector3[mVertexCount];
		for (int i = 0; i < mVertexCount; i++) {
			mNormals[i] = TransformDirection(mesh->mNormals[i], mNormalObjectToWorld);
		}
	}

	mTriangles = new Triangle[mIndiceCount / 3];
	for (int i = 0; i < mIndiceCount / 3; i++) {
		//mTriangles[i].mIndices = &mIndices[i * 3];
		//mTriangles[i].mEntity = this;

		mTriangles[i].Init(this, &mIndices[i * 3]);
	}

}

void MeshEntity::AddToScene(Scene* scene)
{
	for (int i = 0; i < mIndiceCount / 3; i++) {
		Triangle* pTriangle = &mTriangles[i];
		scene->mObjects.push_back(pTriangle);
	}
}

MeshEntity::~MeshEntity()
{
	delete[] mTriangles;
	delete[] mPositions;
	delete[] mNormals;
}

//void MeshEntity::InitVertexBuffers(const Vector3* positions, const Vector3* normals)
//{
//	mPositions = new Vector3[mVertexCount];
//	mNormals = new Vector3[mVertexCount];
//	for (int i = 0; i < mVertexCount; i++) {
//		mPositions[i] = TransformPoint(positions[i], mObjectToWorld);
//		mNormals[i] = TransformDirection(normals[i], mNormalObjectToWorld);
//	}
//
//	mTriangles = new Triangle[mIndiceCount / 3];
//	for (int i = 0; i < mIndiceCount / 3; i++) {
//		mTriangles[i].mIndices = &mIndices[i * 3];
//		mTriangles[i].mMesh = this;
//	}
//}

