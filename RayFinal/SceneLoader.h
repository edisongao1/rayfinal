#pragma once

#include "Vector2.h"
#include "tinyxml2.h"
#include "Matrix4x4.h"
#include "Color.h"

using namespace tinyxml2;

class Scene;
class RenderWindow;
class Material;
class ISceneEntity;

class SceneLoader
{
public:
	SceneLoader(RenderWindow* window);
	~SceneLoader();

	Scene* Load(const char* fileName);
	
	int GetFloatArrayFromString(const char* str, float arr[], int maxNum);
	bool GetVectorFromString(const char* str, Vector3& v);
	bool GetVectorFromString(const char* str, Vector2& v);
	void GetColorFromString(const char* str, Color& color);


private:
	void ParseCamera(Scene* scene, XMLElement* xmlNode);
	void ParseEntity(Scene* scene, XMLElement* xmlNode);
	void ParseLight(Scene* scene, XMLElement* xmlNode);
	void ParseMaterial(XMLElement* xmlNode);

	ISceneEntity* ParsePlaneEntity(Scene* scene, const char* id, XMLElement* xmlNode);
	ISceneEntity* ParseSphereEntity(Scene* scene, const char* id, XMLElement* xmlNode);
	ISceneEntity* ParseDiskEntity(Scene* scene, const char* id, XMLElement* xmlNode);
	void ParseTransform(XMLElement* xmlNode, Matrix4x4& matrix);
	bool GetFloatFromChildText(XMLElement* xmlNode, const char* childTagName, float* val);
	bool GetColorFromChildText(XMLElement* xmlNode, const char* childTagName, Color& color);
	bool GetVectorFromChildText(XMLElement* xmlNode, const char* childTagName, Vector3& v);
	bool GetVectorFromChildText(XMLElement* xmlNode, const char* childTagName, Vector2& v);
	
	RenderWindow*						mWindow;
	std::map<std::string, Material*>	mMaterialMap;

};

