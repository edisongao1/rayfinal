#pragma once
#include "Vector3.h"
#include "MathCommon.h"

class Sampler3D
{
public:
	virtual Vector3 GetSample(float* pdf) = 0;
};

class UniformHemisphereSampler: public Sampler3D
{
public:
	virtual Vector3 GetSample(float* pdf) override;
};

class CosineWeightedHemisphereSampler : public Sampler3D
{
public:
	virtual Vector3 GetSample(float* pdf) override;
};

class DiskSampler: public Sampler3D
{
public:
	DiskSampler(float r = 1.0f) : mRadius(r) {}
	virtual Vector3 GetSample(float* pdf) override;
private:
	float mRadius;
};