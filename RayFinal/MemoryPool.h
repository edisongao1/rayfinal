#pragma once

class LocalThreadMemoryPool
{
public:
	LocalThreadMemoryPool(int nChunkSize) : mChunkSize(nChunkSize)
	{
		memset(mChunkBufferArray, 0, sizeof(mChunkBufferArray));
	}

	void* Malloc()
	{
		//mChunks = malloc(1);
		void* p = mChunkBufferArray[0];
		return malloc(mChunkSize);
	}
	
	void Free(void* p)
	{
		free(p);
	}
private:
	typedef void* ChunkBuffer;
	int									mChunkSize;
	static thread_local ChunkBuffer		mChunkBufferArray[1024];
};


template<typename T, typename...U>
T* CreateObject(U... args)
{
	LocalThreadMemoryPool pool(sizeof(T));
	void* p = pool.Malloc();
	return new(p) T(args...);
}

template<typename T>
void ReleaseObject(T* p)
{
	delete p;
}
