#pragma once
#include "Vector2.h"
#include "Shape.h"


class TriangleMesh
{
public:

	TriangleMesh(const std::string& name)
		:mName(name)
		,mPositions(nullptr)
		,mNormals(nullptr)
		,mUVs(nullptr)
		,mIndices(nullptr)
		,mVertexCount(0)
		,mIndiceCount(0)
	{

	}

	const std::string& GetName() const {
		return mName;
	}

public:
	std::string		mName;
	Vector3*		mPositions;
	Vector3*		mNormals;
	int				mVertexCount;
	int*			mIndices;
	int				mIndiceCount;
	SUVCoordinate*  mUVs;
};

class MeshManager
{
public:
	MeshManager();
	static MeshManager& Get();
	void PreloadMeshes();
	TriangleMesh* GetMesh(const std::string& name);
	TriangleMesh* CreatePlaneMesh(const std::string& name, Vector2 size);
private:
	void CreatePlaneMesh();
	

	TriangleMesh* mPlaneMesh;
	std::map<std::string, TriangleMesh*> mMeshMap;
};

//class Sphere : public IHitable
//{
//public:
//	Sphere(const Vector3& center, float r)
//		:center(center), radius(r)
//	{
//
//	}
//
//	virtual bool Hit(const Ray& ray, IntersectResult& result) const
//	{
//		float a = ray.d.squaredNorm();
//		float b = 2 * (ray.o - center).dot(ray.d);
//		float c = center.squaredNorm() - radius * radius;
//		float delta = b * b - 4 * a * c;
//		if (delta < 0)
//			return false;
//		float sqrt_delta = sqrtf(delta);
//		float t1 = (-b - sqrt_delta) / (2 * a);
//		if (t1 > ray.maxt)
//			return false;
//
//		if (t1 < ray.mint)
//		{
//			float t2 = (-b + sqrt_delta) / (2 * a);
//			if (t2 >= ray.mint && t2 <= ray.maxt)
//			{
//				t1 = t2;
//			}
//			else
//			{
//				return false;
//			}
//		}
//
//		result.pos = ray(t1);
//		result.normal = (result.pos - center).normalized();
//		result.t = t1;
//		return true;
//	}
//
//private:
//	Vector3		center;
//	float		radius;
//};
