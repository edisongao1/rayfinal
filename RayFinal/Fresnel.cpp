#include "pch.h"
#include "Fresnel.h"
#include "MathLib.h"

FresnelDielectric::FresnelDielectric(float ei, float et)
	:etai(ei), etat(et)
{

}

float FresnelDielectric::Evaluate(float cosi)
{
	bool entering = cosi > 0;
	float ei = etai, et = etat;
	if (!entering) {
		std::swap(ei, et);
		cosi = fabs(cosi);
	}
	float sint2 = (ei * ei) * (1.0f - cosi * cosi) / (et * et);
	if (sint2 > 1.0f)
		return 1;
	float cost = sqrtf(1.0f - sint2);
	
	float rp = (et * cosi - ei * cost) / (et * cosi + ei * cost);
	float rv = (ei * cosi - et * cost) / (ei * cosi + et * cost);
	
	return 0.5f * (rp * rp + rv * rv);
}

FresnelConductor::FresnelConductor(float eta, float k)
	:eta(eta), k(k)
{

}

float FresnelConductor::Evaluate(float cosi)
{
	float t1 = (eta * eta + k * k);
	float t2 = cosi * cosi;
	
	float rp2 = (t1*t2 - 2*eta*cosi + 1) / (t1*t2 + 2*eta*cosi + 1);
	float rv2 = (t1 - 2*eta*cosi + t2) / (t1 + 2*eta*cosi + t2);

	return 0.5f * (rp2 + rv2);
}

