#pragma once
#include "MathLib.h"
#include "Color.h"

class RenderWindow;
class Scene;
class Ray;
class BxDF;
struct DifferentialGeometry;

enum class EUpdateSurfaceFrequency
{
	eUpdateSurface_Whole,
	eUpdateSurface_PerBlock,
	eUpdateSurface_PerPixel
};

struct SRenderSettings
{
	int							BlockSize;	
	EUpdateSurfaceFrequency		UpdateSurfaceFrequency;
	int							SamplesPerPixel;
	Color						BackgroundColor;
	int							MaxThreadCount;

	int							MaxIndirectLightingDepth;
	
	// number of rays generated for indirect lighting each time
	int							IndirectLightingRayCount;

	SRenderSettings()
	{
		BlockSize = 32;
		UpdateSurfaceFrequency = EUpdateSurfaceFrequency::eUpdateSurface_PerBlock;
		SamplesPerPixel = 8;
		BackgroundColor = Color(0, 0, 0, 1);
		MaxIndirectLightingDepth = 3;
		IndirectLightingRayCount = 2;
	}
};


class PathTracer
{
	enum { MAX_THREAD_NUM = 32 };
public:
	PathTracer(RenderWindow* renderWindow, Scene* scene, const SRenderSettings& settings);
	~PathTracer();

	void Render();
	void RayTraceThreadEntry();

	Color PixelTraceSamples(int x, int y);
	Color TraceRay(const Ray& ray);

private:

	Color IndirectLighting(const Ray& ray, const DifferentialGeometry& dg, 
		BxDF* bxdf, const Vector3& wo);

	SRenderSettings		mRenderSettings;

	std::thread			mRayTraceThreads[MAX_THREAD_NUM];
	int					mRayTraceThreadCount;
	RenderWindow*		mRenderWindow;
	Scene*				mScene;

	int					mBlockRowCount;
	int					mBlockColCount;
	std::atomic<int>	mCurBlockIndex;
};