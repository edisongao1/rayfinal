#ifndef __VECTOR3_CLASS_H__
#define __VECTOR3_CLASS_H__
#include <math.h>
#include "MathCommon.h"

template<class T>
class TVector3
{
public:
	const static int DIMENSION = 3;
	T x, y, z;

	TVector3()
	{
		x = 0;
		y = 0;
		z = 0;
	}

	TVector3& operator=(const TVector3& t)
	{
		x = t.x;
		y = t.y;
		z = t.z;
		return *this;
	}

	TVector3(T x, T y, T z) :x(x), y(y), z(z) {}

	bool operator==(const TVector3& t) const {
		return x == t.x && y == t.y && z == t.z;
	}

	bool operator !=(const TVector3 &a) const {
		return x != a.x || y != a.y || z != a.z;
	}
	
	TVector3 operator-() const { return TVector3(-x, -y, -z); }

	TVector3 operator+(T k) const  {
		return TVector3(x + k, y + k, z + k);
	}
	TVector3 operator+(const TVector3 &a) const {
		return TVector3(x + a.x, y + a.y, z + a.z);
	}

	TVector3 operator-(T k) const { 
		return TVector3(x - k, y - k, z - k);
	}
	TVector3 operator-(const TVector3 &a) const {
		return TVector3(x - a.x, y - a.y, z - a.z);
	}
	
	TVector3 operator*(T k) const {
		return TVector3(x*k, y*k, z*k);
	}

	TVector3 operator *(const TVector3 &a) const {
		return TVector3(x * a.x, y * a.y, z * a.z);
	}

	TVector3 operator /(T a) const {
		auto oneOverA = recip_tpl(a); // NOTE: no check for divide by zero here
		return TVector3<T>(x*oneOverA, y*oneOverA, z*oneOverA);
	}

	TVector3& operator +=(const TVector3 &a) {
		x += a.x; y += a.y; z += a.z;
		return *this;
	}

	TVector3& operator +=(T k) {
		x += k; y += k; z += k;
		return *this;
	}

	TVector3& operator -=(const TVector3 &a) {
		x -= a.x; y -= a.y; z -= a.z;
		return *this;
	}

	TVector3& operator -=(T k) {
		x -= k; y -= k; z -= k;
		return *this;
	}

	TVector3& operator *=(T a) {
		x *= a; y *= a; z *= a;
		return *this;
	}

	TVector3& operator /=(T a) {
		auto oneOverA = recip_tpl(a);
		x *= oneOverA; y *= oneOverA; z *= oneOverA;
		return *this;
	}

	void Normalize() {
		T magSq = LengthSquared();
		if (magSq > 0)
		{
			// check for divide-by-zero
			T oneOverMag = recip_tpl(sqrt_tpl(magSq));
			x *= oneOverMag;
			y *= oneOverMag;
			z *= oneOverMag;
		}
	}

	TVector3 GetNormalized() const {
		TVector3<T> v(*this);
		v.Normalize();
		return v;
	}

	auto Length() const {
		return (T)sqrt_tpl(x * x + y * y + z * z);
	}

	T LengthSquared() const {
		return x * x + y * y + z * z;
	}

	T Dot(const TVector3& a) const {
		return x * a.x + y * a.y + z * a.z;
	}

	TVector3 Cross(const TVector3 &b) const {
		return TVector3(
			y*b.z - z * b.y,
			z*b.x - x * b.z,
			x*b.y - y * b.x
		);
	}

	auto Distance(const TVector3 &b) const {
		T dx = x - b.x;
		T dy = y - b.y;
		T dz = z - b.z;
		return sqrt_tpl(dx * dx + dy * dy + dz * dz);
	}

	T DistanceSquared(const TVector3& b) const {
		T dx = x - b.x;
		T dy = y - b.y;
		T dz = z - b.z;
		return (dx*dx + dy * dy + dz * dz);
	}

	T DistanceSquared2D(const TVector3& b) const {
		T dx = x - b.x;
		T dy = y - b.y;
		return (dx * dx + dy * dy);
	}

	auto Distance2D(const TVector3& b) const {
		T dx = x - b.x;
		T dy = y - b.y;
		return sqrt_tpl(dx * dx + dy * dy);
	}
};

template<class T>
inline TVector3<T> Cross(const TVector3<T> &a, const TVector3<T> &b) {
	return TVector3<T>(
		a.y*b.z - a.z*b.y,
		a.z*b.x - a.x*b.z,
		a.x*b.y - a.y*b.x
		);
}

template<class T>
inline T Dot(const TVector3<T> &a, const TVector3<T> &b)
{
	return a.Dot(b);
}

template<>
inline bool TVector3<float>::operator==(const TVector3<float>& t) const {
	return fcmp_tpl(x, t.x) && fcmp_tpl(y, t.y) && fcmp_tpl(z, t.z);
}

template<>
inline bool TVector3<double>::operator==(const TVector3<double>& t) const {
	return fcmp_tpl(x, t.x) && fcmp_tpl(y, t.y) && fcmp_tpl(z, t.z);
}

template<>
inline bool TVector3<float>::operator!=(const TVector3<float>& t) const {
	return !fcmp_tpl(x, t.x) || !fcmp_tpl(y, t.y) || !fcmp_tpl(z, t.z);
}

template<>
inline bool TVector3<double>::operator!=(const TVector3<double>& t) const {
	return !fcmp_tpl(x, t.x) || !fcmp_tpl(y, t.y) || !fcmp_tpl(z, t.z);
}

template<class T>
inline TVector3<T> operator*(T k, const TVector3<T>& v)
{
	return TVector3<T>(v.x*k, v.y*k, v.z*k);
}

using Vector3 = TVector3<float>;
using Vector3f = TVector3<float>;
using Vector3i = TVector3<int>;
using Vector3d = TVector3<double>;


#endif