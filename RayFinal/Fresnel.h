#pragma once
class Fresnel
{
public:
	virtual float Evaluate(float cosi) = 0;
};


class FresnelDielectric : public Fresnel
{
public:
	FresnelDielectric(float ei, float et);
	virtual float Evaluate(float cosi) override;

private:
	float etai, etat;
};

class FresnelConductor : public Fresnel
{
public:
	FresnelConductor(float eta, float k);
	virtual float Evaluate(float cosi) override;

private:
	float eta, k;
};
