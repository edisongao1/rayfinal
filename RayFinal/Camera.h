#pragma once

#include "Vector2.h"
#include "Ray.h"

class Camera
{
public:
	//Camera(Vector3i screenSize, float fov, float aspect);
	Ray GenerateRay(int x, int y) const;
	//void Init(Vector2i screenSize, float fov);
	void Init(const Vector3& pos, const Vector3& lookAt, 
		const Vector2i& screenSize, float fov, float nearZ, float farZ);



private:
	Vector2		mProjPlaneSize;
	Vector2i	mScreenSize;
	float		mFov;
	float		mAspectRatio;
	float		mNearZ;
	float		mFarZ;
	Vector3		mPosition;

	Matrix4x4	mWorldToView;
	Matrix4x4	mViewToWorld;

	Matrix4x4	mViewToProj;
	Matrix4x4	mProjToView;

	Matrix4x4	mProjToWorld;
	Matrix4x4	mWorldToProj;
};