#pragma once

#include <vector>

#include "Shape.h"
#include "Camera.h"

class SceneLoader;
class ILight;

class Scene
{
public:
	Scene();
	ISceneEntity* RayIntersect(const Ray& ray, DifferentialGeometry& result);
	bool RayIntersectFast(const Ray& ray);

	//bool RayIntersect(const Ra)

	void AddEntity(ISceneEntity* pObject);
	void AddLight(ILight* pLight);
public:
	std::vector<IPrimitive*>	mObjects;
	Camera						mCamera;

public:
	std::vector<ILight*>		mLights;
};

