#include "pch.h"
#include "MicrofacetDistribution.h"

float MicrofacetDistribution::G(const Vector3& wi, const Vector3& wo, const Vector3& wh) const
{
	float woDotWh = wo.Dot(wh);
	if (woDotWh == 0.0f)
		return 1.0f;

	float t1 = 2 * (wh.z) * (wo.z) / woDotWh;
	float t2 = 2 * (wh.z) * (wi.z) / woDotWh;

	return std::min<float>(1.0f, std::min<float>(t1, t2));
}

float BlinnDistribution::D(const Vector3& wh) const
{
	float cosh = fabs(wh.z);
	return (mExponent + 2) * INV_2PI * pow(cosh, mExponent);
}

void BlinnDistribution::Sample_D(const Vector3& wo, Vector3* wi, float* pdf) const
{
	float e1 = rand_float();
	float e2 = rand_float();

	float cosThetaH = pow(e1, 1.0f / (mExponent + 1.0f));
	float phiH = _2PI * e2;

	float sinThetaH = sqrtf(1.0f - cosThetaH * cosThetaH);
	float cosPhiH = cosf(phiH);
	float sinPhiH = sinf(phiH);

	Vector3 wh;
	wh.x = sinThetaH * cosPhiH;
	wh.y = sinThetaH * sinPhiH;
	wh.z = cosThetaH;

	*wi = 2 * Dot(wo, wh) * wh - wo;
	float woDotWh = wo.Dot(wh);
	if (woDotWh == 0.0f) {
		*pdf = 1.0f;
	}
	else {
		*pdf = (mExponent + 1) * INV_2PI * pow(cosThetaH, mExponent) / (4 * woDotWh);
	}
}
