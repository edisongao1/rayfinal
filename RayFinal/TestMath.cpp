#include "pch.h"
#include <Windows.h>
#include <DirectXMath.h>
#include "MathCommon.h"
#include "Matrix4x4.h"
using namespace DirectX;


void VectorEqual(Vector3 a, XMFLOAT3 b) {
	assert(fcmp_tpl(a.x, b.x));
	assert(fcmp_tpl(a.y, b.y));
	assert(fcmp_tpl(a.z, b.z));
}

void VectorEqual(Vector3 a, FXMVECTOR v) {
	XMFLOAT3 b;
	XMStoreFloat3(&b, v);
	assert(fcmp_tpl(a.x, b.x));
	assert(fcmp_tpl(a.y, b.y));
	assert(fcmp_tpl(a.z, b.z));
}

void TestVector3()
{
	float a[100];
	for (int i = 0; i < 100; i++) {
		a[i] = rand_float();
	}

	Vector3 v1(a[0], a[1], a[2]);
	Vector3 v2(a[3], a[4], a[5]);
	XMFLOAT3 mv1(a[0], a[1], a[2]);
	XMFLOAT3 mv2(a[3], a[4], a[5]);
	XMVECTOR xv1 = XMLoadFloat3(&mv1);
	XMVECTOR xv2 = XMLoadFloat3(&mv2);
	// add
	Vector3 v3 = v1 + v2;
	XMVECTOR xv3 = xv1 + xv2;
	//XMFLOAT3 mv3;
	//XMStoreFloat3(&mv3, xv3);
	VectorEqual(v3, xv3);
	
	//XMFLOAT3 mv1(a, b, c);
	//v1 = v1 + v1;
	
}

void TestMatrix4x4()
{
	float a[100];
	for (int i = 0; i < 100; i++) {
		a[i] = rand_float();
	}
	Matrix4x4 m;
	XMFLOAT4X4 mm;
	
}

class TestMathHelper
{
public:
	TestMathHelper()
	{
		TestVector3();
		int b = 1;
	}
};

TestMathHelper helper;



