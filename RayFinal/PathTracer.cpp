#include "pch.h"
#include "PathTracer.h"
#include "Scene.h"
#include "RenderWindow.h"
#include "Light.h"
#include "Material.h"
#include "BSDF.h"


PathTracer::PathTracer(RenderWindow* renderWindow, Scene* scene, const SRenderSettings& settings)
	:mRenderWindow(renderWindow)
	, mScene(scene)
	, mRenderSettings(settings)
	, mCurBlockIndex(0)
{
	mBlockColCount = mRenderWindow->GetWidth() / mRenderSettings.BlockSize;
	if (mBlockColCount * mRenderSettings.BlockSize != mRenderWindow->GetWidth()) {
		mBlockColCount += 1;
	}
	mBlockRowCount = mRenderWindow->GetHeight() / mRenderSettings.BlockSize;
	if (mBlockRowCount * mRenderSettings.BlockSize != mRenderWindow->GetHeight()) {
		mBlockRowCount += 1;
	}
}

PathTracer::~PathTracer()
{
	for (int i = 0; i < mRayTraceThreadCount; i++) {
		mRayTraceThreads[i].join();
	}
}

void PathTracer::Render()
{
	mCurBlockIndex.store(0);
	mRayTraceThreadCount = std::min<int>(mRenderSettings.MaxThreadCount, 
		std::thread::hardware_concurrency() - 1);

	if (mRayTraceThreadCount > MAX_THREAD_NUM) {
		mRayTraceThreadCount = MAX_THREAD_NUM;
	}

	for (int i = 0; i < mRayTraceThreadCount; i++) {
		mRayTraceThreads[i] = std::thread(std::mem_fn(&PathTracer::RayTraceThreadEntry), this);
	}
}

void PathTracer::RayTraceThreadEntry()
{
	int blockSize = mRenderSettings.BlockSize;
	Color* pColorBuffer = new Color[blockSize * blockSize];
	while (true) {
		int blockIndex = mCurBlockIndex.fetch_add(1);
		if (blockIndex >= mBlockColCount * mBlockRowCount) {
			break;
		}
		Rect rect;
		
		int blockRow = blockIndex / mBlockColCount;
		int blockCol = blockIndex - blockRow * mBlockColCount;
		rect.left = blockCol * blockSize;
		rect.right = std::min<int>(rect.left + blockSize, mRenderWindow->GetWidth());
		rect.top = blockRow * blockSize;
		rect.bottom = std::min<int>(rect.top + blockSize, mRenderWindow->GetHeight());
		
		Color* pCurrentColor = pColorBuffer;
		for (int y = rect.top; y < rect.bottom; y++) {
			for (int x = rect.left; x < rect.right; x++) {
				*pCurrentColor = PixelTraceSamples(x, y);

				if (mRenderSettings.UpdateSurfaceFrequency == EUpdateSurfaceFrequency::eUpdateSurface_PerPixel)
					mRenderWindow->SetPixel(*pCurrentColor, x, y);

				pCurrentColor += 1;
			}
		}
		if (mRenderSettings.UpdateSurfaceFrequency == EUpdateSurfaceFrequency::eUpdateSurface_PerBlock)
			mRenderWindow->SetPixels(pColorBuffer, rect);
	}

	delete[] pColorBuffer;

	//for (int i = 0; i < 1000; i++) {
	//	printf("RayTraceThreadEntry %d %d\n", i, std::this_thread::get_id());
	//	std::this_thread::sleep_for(std::chrono::milliseconds(rand_int(0, 1000)));
	//}
}

Color PathTracer::PixelTraceSamples(int x, int y)
{
	Color sum(0, 0, 0, 0);
	const int n = mRenderSettings.SamplesPerPixel;
	for (int i = 0; i < mRenderSettings.SamplesPerPixel; i++)
	{
		Ray ray = mScene->mCamera.GenerateRay(x, y);
		sum += TraceRay(ray);
	}
	sum /= (float)n;
	sum.a = 1.0f;
	return sum;
}

Color PathTracer::TraceRay(const Ray& ray)
{
	DifferentialGeometry dg;
	ISceneEntity* pSceneEntity = mScene->RayIntersect(ray, dg);
	if (pSceneEntity == nullptr)
		return mRenderSettings.BackgroundColor;

	Color sumColor;
	Vector3 wo = dg.WorldToLocal(-ray.d);
	Material* pMaterial = pSceneEntity->GetMaterial();
	sumColor += pMaterial->GetEmission();
	
	BxDF* brdf = pMaterial->GetBRDF(dg);
	BxDF* btdf = pMaterial->GetBTDF(dg);

	if (brdf)
	{
		// 镜面材质不计算直接光照
		if (!brdf->IsDelta())
		{
			// direct lighting
			for (ILight* pLight : mScene->mLights) {
				Vector3 wwi;
				float fDistToLight;
				float pdf;
				Color radiance = pLight->Sample_L(dg.pos, &wwi, &fDistToLight, &pdf);
				Ray rayToLight(dg.pos, wwi, 0.0001f, fDistToLight - 0.0001f);
				Vector3 wi = dg.WorldToLocal(wwi);

				if (wi.z > 0 && !mScene->RayIntersectFast(rayToLight)) {
					Color color = brdf->f(wo, wi) * radiance * fabs(wi.z) / pdf;
					sumColor += color.Clamp();
					//sumColor += brdf->f(wo, wi) * radiance * fabs(wi.z) / pdf;
				}
			}
		}

		// indirect lighting
		if (ray.depth < mRenderSettings.MaxIndirectLightingDepth)
		{
			Color indirectLightingColor = IndirectLighting(ray, dg, brdf, wo);
			sumColor += indirectLightingColor.Clamp();
		}
	}

	if (btdf)
	{
		// indirect lighting
		if (ray.depth < mRenderSettings.MaxIndirectLightingDepth)
		{
			Color indirectLightingColor = IndirectLighting(ray, dg, btdf, wo);
			sumColor += indirectLightingColor.Clamp();
		}
	}
	
	return sumColor;
}

Color PathTracer::IndirectLighting(const Ray& ray, const DifferentialGeometry& dg,
	BxDF* bxdf, const Vector3& wo)
{
	Color indirectLightingColor;
	int iSampleCount = bxdf->IsDelta() ? 1 : mRenderSettings.IndirectLightingRayCount;

	for (int i = 0; i < iSampleCount; i++) {
		Vector3 wi;
		float pdf;
		Color fBRDF = bxdf->Sample_f(wo, &wi, &pdf);
		if (fBRDF.IsZero())
			continue;
		
		Vector3 wwi = dg.LocalToWorld(wi);
		Ray nextRay(dg.pos, wwi, 1e-5f);
		nextRay.depth = ray.depth + 1;
		indirectLightingColor += fBRDF * TraceRay(nextRay) * fabs(wi.z) / pdf;
	}
	indirectLightingColor /= iSampleCount;

	return indirectLightingColor;
}

