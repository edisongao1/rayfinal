#include "pch.h"
#include "RenderWindow.h"

RenderWindow::RenderWindow(int width, int height, bool EnableGamma)
	:m_pPixelsData(nullptr)
	, m_pSDLWindow(nullptr)
	, m_pScreenSurface(nullptr)
	, m_bQuit(false)
	, m_nWidth(width)
	, m_nHeight(height)
	, m_bEnableGamma(EnableGamma)
{
	m_pPixelsData = new Uint32[width * height];
	memset(m_pPixelsData, 0, width * height * sizeof(Uint32));
}

RenderWindow::~RenderWindow()
{
	delete[] m_pPixelsData;

	m_pScreenSurface = nullptr;
	SDL_FreeSurface(m_pDrawableSurface);
	m_pDrawableSurface = nullptr;

	SDL_DestroyWindow(m_pSDLWindow);
	m_pSDLWindow = nullptr;
}

bool RenderWindow::Init()
{
	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		return false;
	}

	//Create window
	m_pSDLWindow = SDL_CreateWindow("SDL App",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, m_nWidth, m_nHeight, SDL_WINDOW_SHOWN);
	if (m_pSDLWindow == NULL)
	{
		printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		return false;
	}

	m_pScreenSurface = SDL_GetWindowSurface(m_pSDLWindow);
	
	m_pDrawableSurface = SDL_CreateRGBSurface(0, m_nWidth, m_nHeight, 32, 0, 0, 0, 0);
	if (m_pDrawableSurface == NULL)
	{
		printf("Unable to create background surface %s! SDL Error: %s\n", "03_event_driven_programming/x.bmp", SDL_GetError());
		return false;
	}
	
	return true;
}


Uint32 RenderWindow::ConvertColorToUint32(const Color& color)
{
	Color c = color;

	if (m_bEnableGamma)
	{
		float gamma = 2.2f;
		float level = 1.0f;
		float one_over_gamma = 1.0f / gamma;
		float exposure = sqrt(pow(2, level));

		c.r = pow(c.r * exposure, one_over_gamma);
		c.g = pow(c.g * exposure, one_over_gamma);
		c.b = pow(c.b * exposure, one_over_gamma);
	}

	Uint8 r = static_cast<Uint8>(clamp_tpl(c.r, 0.0f, 1.0f) * 255.99f);
	Uint8 g = static_cast<Uint8>(clamp_tpl(c.g, 0.0f, 1.0f) * 255.99f);
	Uint8 b = static_cast<Uint8>(clamp_tpl(c.b, 0.0f, 1.0f) * 255.99f);
	Uint8 a = static_cast<Uint8>(clamp_tpl(c.a, 0.0f, 1.0f) * 255.99f);
	return SDL_MapRGBA(m_pDrawableSurface->format, r, g, b, a);
}


void RenderWindow::SetPixels(Color* data)
{
	std::lock_guard<std::mutex> lock(m_pixelDataMutex);
	for (int i = 0; i < m_nWidth * m_nHeight; i++) {
		/*Uint8 r = static_cast<Uint8>(data[i].r * 255.99f);
		Uint8 g = static_cast<Uint8>(data[i].g * 255.99f);
		Uint8 b = static_cast<Uint8>(data[i].b * 255.99f);
		Uint8 a = static_cast<Uint8>(data[i].a * 255.99f);
		m_pPixelsData[i] = SDL_MapRGBA(m_pDrawableSurface->format, r, g, b, a);*/
		m_pPixelsData[i] = ConvertColorToUint32(data[i]);
	}
}

void RenderWindow::SetPixels(Color* data, Rect region)
{
	Color* pColor = data;
	std::lock_guard<std::mutex> lock(m_pixelDataMutex);
	for (int y = region.top; y < region.bottom; y++) {
		for (int x = region.left; x < region.right; x++) {
			int i = y * m_nWidth + x;
			/*Uint8 r = static_cast<Uint8>(pColor->r * 255.99f);
			Uint8 g = static_cast<Uint8>(pColor->g * 255.99f);
			Uint8 b = static_cast<Uint8>(pColor->b * 255.99f);
			Uint8 a = static_cast<Uint8>(pColor->a * 255.99f);
			m_pPixelsData[i] = SDL_MapRGBA(m_pDrawableSurface->format, r, g, b, a);*/
			m_pPixelsData[i] = ConvertColorToUint32(*pColor);
			pColor += 1;
		}
	}
}

void RenderWindow::SetPixel(Color color, int x, int y)
{
	int i = y * m_nWidth + x;
	//Uint8 r = static_cast<Uint8>(color.r * 255.99f);
	//Uint8 g = static_cast<Uint8>(color.g * 255.99f);
	//Uint8 b = static_cast<Uint8>(color.b * 255.99f);
	//Uint8 a = static_cast<Uint8>(color.a * 255.99f);

	std::lock_guard<std::mutex> lock(m_pixelDataMutex);
	//m_pPixelsData[i] = SDL_MapRGBA(m_pDrawableSurface->format, r, g, b, a);
	m_pPixelsData[i] = ConvertColorToUint32(color);
}

void RenderWindow::Loop()
{
	SDL_Event e;

	//While application is running
	while (!m_bQuit)
	{
		//Handle events on queue
		while (SDL_PollEvent(&e) != 0)
		{
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				m_bQuit = true;
			}
		}

		DrawFrame();
	}
}

void RenderWindow::DrawFrame()
{
	SDL_LockSurface(m_pDrawableSurface);
	Uint32* data = reinterpret_cast<Uint32*>(m_pDrawableSurface->pixels);
	{
		std::lock_guard<std::mutex> lock(m_pixelDataMutex);
		Uint32* pCurColor = m_pPixelsData;
		for (int y = 0; y < m_nHeight; y++) {
			for (int x = 0; x < m_nWidth; x++) {
				int i = y * m_pDrawableSurface->pitch / sizeof(Uint32) + x;
				data[i] = *pCurColor;
				pCurColor += 1;
			}
		}
	}
	
	SDL_UnlockSurface(m_pDrawableSurface);
	SDL_BlitSurface(m_pDrawableSurface, NULL, m_pScreenSurface, NULL);

	//Update the surface
	SDL_UpdateWindowSurface(m_pSDLWindow);
}

