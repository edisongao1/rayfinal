#include "pch.h"
#include "MathCommon.h"

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<float> gFloatDis(0.0f, 1.0f);

float rand_float()
{
	return gFloatDis(gen);
}

float rand_float(float a, float b)
{
	return a + (b - a) * gFloatDis(gen);
}

int rand_int(int a, int b)
{
	std::uniform_int_distribution<int> dis(a, b);
	return dis(gen);
}

