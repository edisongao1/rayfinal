#pragma once
#include "MathCommon.h"
#include "Ray.h"

class Scene;
class Material;
class ISceneEntity;

struct SUVCoordinate
{
	float u, v;
};


struct DifferentialGeometry
{
	float t;
	float u, v;

	Vector3 pos;

	Vector3 ns;
	Vector3 ts;
	Vector3 bs;

	Vector3 dpdu;
	Vector3 dpdv;

	void WorldToLocal(const Vector3& v, Vector3& out) const
	{
		out.x = ts.x * v.x + ts.y * v.y + ts.z * v.z;
		out.y = bs.x * v.x + bs.y * v.y + bs.z * v.z;
		out.z = ns.x * v.x + ns.y * v.y + ns.z * v.z;
	}

	Vector3 WorldToLocal(const Vector3& v) const
	{
		Vector3 out;
		out.x = ts.x * v.x + ts.y * v.y + ts.z * v.z;
		out.y = bs.x * v.x + bs.y * v.y + bs.z * v.z;
		out.z = ns.x * v.x + ns.y * v.y + ns.z * v.z;
		return out;
	}

	void LocalToWorld(const Vector3& v, Vector3& out) const
	{
		out.x = ts.x * v.x + bs.x * v.y + ns.x * v.z;
		out.y = ts.y * v.x + bs.y * v.y + ns.y * v.z;
		out.z = ts.z * v.x + bs.z * v.y + ns.z * v.z;
	}

	Vector3 LocalToWorld(const Vector3& v) const
	{
		Vector3 out;
		out.x = ts.x * v.x + bs.x * v.y + ns.x * v.z;
		out.y = ts.y * v.x + bs.y * v.y + ns.y * v.z;
		out.z = ts.z * v.x + bs.z * v.y + ns.z * v.z;
		return out;
	}
};

class IPrimitive
{
public:
	virtual bool RayIntersect(const Ray& ray, DifferentialGeometry& result) = 0;
	virtual bool RayIntersectFast(const Ray& ray, float& t) = 0;
	virtual ISceneEntity* GetSceneEntity() = 0;
};

class ISceneEntity
{
public:
	ISceneEntity(const Matrix4x4& worldTransform)
		:mObjectToWorld(worldTransform)
		, mWorldToObject(worldTransform.GetInverted())
		, mMaterial(nullptr)
	{

	}
	virtual void AddToScene(Scene* scene) = 0;
	virtual Material* GetMaterial() { return mMaterial; }
	virtual void SetMaterial(Material* material) { mMaterial = material; }
protected:
	Matrix4x4	mObjectToWorld;
	Matrix4x4	mWorldToObject;
	Material*	mMaterial;
};

class Sphere : public IPrimitive, public ISceneEntity
{
public:
	Sphere(float radius, const Matrix4x4& worldTransform)
		:ISceneEntity(worldTransform)
		,mRadius(radius)
	{

	}

	virtual bool RayIntersect(const Ray& ray, DifferentialGeometry& result) override;
	virtual bool RayIntersectFast(const Ray& ray, float& t) override;
	virtual void AddToScene(Scene* scene) override;
	virtual ISceneEntity* GetSceneEntity() { return this; }

private:
	float		mRadius;
};

class Disk : public IPrimitive, public ISceneEntity
{
public:
	Disk(float radius, const Matrix4x4& worldTransform)
		:ISceneEntity(worldTransform)
		, mRadius(radius)
	{

	}

	virtual bool RayIntersect(const Ray& ray, DifferentialGeometry& result) override;
	virtual bool RayIntersectFast(const Ray& ray, float& t) override;
	virtual ISceneEntity* GetSceneEntity() { return this; }
	virtual void AddToScene(Scene* scene) override;
private:
	float		mRadius;
};
