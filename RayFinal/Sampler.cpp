#include "pch.h"
#include "Sampler.h"

Vector3 UniformHemisphereSampler::GetSample(float* pdf)
{
	float costheta = rand_float();
	//float costheta = (float)(std::rand()) / RAND_MAX;
	//float costheta = rand() % 1000 * 0.001f;

	float sintheta = sqrtf(1 - costheta * costheta);

	float phi = _2PI * rand_float();
	//float phi = rand() % 1000 * 0.001f;
	Vector3 v;
	v.x = sintheta * cosf(phi);
	v.y = sintheta * sinf(phi);
	v.z = costheta;

	*pdf = INV_2PI;
	return v;
}

Vector3 CosineWeightedHemisphereSampler::GetSample(float* pdf)
{
	float e1 = rand_float();
	float e2 = rand_float();
	float sintheta = sqrtf(e1);
	float costheta = sqrtf(1 - e1);

	sintheta = clamp_tpl<float>(sintheta, 0, 1);
	costheta = clamp_tpl<float>(costheta, 0, 1);

	float phi = _2PI * e2;

	Vector3 v;
	v.x = sintheta * cosf(phi);
	v.y = sintheta * sinf(phi);
	v.z = costheta;

	*pdf = costheta * INV_PI;
	*pdf = max(0.0001f, *pdf);
	return v;
}

Vector3 DiskSampler::GetSample(float* pdf)
{
	float e1 = rand_float();
	float e2 = rand_float();

	float r = sqrtf(e1) * mRadius;
	float phi = _2PI * e2;

	Vector3 v;
	v.x = r * cosf(phi);
	v.y = r * sinf(phi);
	v.z = 0;

	*pdf = 1.0f / (PI * mRadius * mRadius);
	return v;
}

