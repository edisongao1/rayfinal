#include "pch.h"
#include "Camera.h"

//Camera::Camera(Vector3i screenSize, float fov, float aspect)
//	:m_screenSize(screenSize), m_fov(fov), m_aspect(aspect)
//{
//	float h = 2 * tanf(mFov / 2);
//	float w = h * mAspect;
//
//	m_projPlaneSize.x = w;
//	m_projPlaneSize.y = h;
//}

Ray Camera::GenerateRay(int sx, int sy) const
{
	//Vector2 offset(rand_float(), rand_float());
	//float x = ((sx + offset.x) / (float)mScreenSize.x - 0.5f) * mProjPlaneSize.x;
	//float y = (0.5f - (sy + offset.y) / (float)mScreenSize.y) * mProjPlaneSize.y;

	//Ray ray;
	//ray.o = Vector3(0, 0, 0);
	//ray.d = Vector3(x, y, -1.0f).GetNormalized();
	//ray.mint = 0;
	//ray.maxt = FLT_MAX;
	//return ray;

	Vector2 offset(rand_float(), rand_float());
	float x = (sx + offset.x) / (float)mScreenSize.x * 2.0f - 1.0f;
	float y = 1.0f - (sy + offset.y) / (float)mScreenSize.y * 2.0f;

	Vector3 projPos(x, y, 0);
	Vector3 rayTargetPos = TransformPointW(projPos, mProjToWorld);
	Ray ray;
	ray.o = mPosition;
	ray.d = (rayTargetPos - mPosition).GetNormalized();
	ray.mint = 0;
	ray.maxt = FLT_MAX;
	return ray;
}

//void Camera::Init(Vector2i screenSize, float fov)
//{
//	mScreenSize = screenSize;
//	mFov = fov;
//	mAspectRatio = static_cast<float>(screenSize.x) / static_cast<float>(screenSize.y);
//
//	float h = 2 * tanf(mFov / 2);
//	float w = h * mAspectRatio;
//
//	mProjPlaneSize.x = w;
//	mProjPlaneSize.y = h;
//}

void Camera::Init(const Vector3& pos, const Vector3& viewDir, 
	const Vector2i& screenSize, float fov, float nearZ, float farZ)
{
	//nearZ = -nearZ;
//	farZ = -farZ;

	mPosition = pos;
	mScreenSize = screenSize;
	mFov = fov;
	mNearZ = nearZ;
	mFarZ = farZ;
	mAspectRatio = (float)mScreenSize.x / (float)mScreenSize.y;

	const Vector3 UP_Y(0, 1.0f, 0);
	Vector3 dir = viewDir.GetNormalized();
	Vector3 t = -dir;
	Vector3 u = UP_Y.Cross(t).GetNormalized();
	Vector3 v = t.Cross(u).GetNormalized();

	float h = 2 * tanf(mFov / 2);
	float w = h * mAspectRatio;
	
	mProjPlaneSize.x = w;
	mProjPlaneSize.y = h;

	mViewToWorld = Matrix4x4(
		u.x, v.x, t.x, pos.x,
		u.y, v.y, t.y, pos.y,
		u.z, v.z, t.z, pos.z,
		0, 0, 0, 1
	);

	mWorldToView = mViewToWorld.GetInverted();

	float tanHalfFov = std::tanf(fov / 2.0f);

	mViewToProj = Matrix4x4(
		1.0f / (mAspectRatio * tanHalfFov), 0, 0, 0,
		0, 1.0f / tanHalfFov, 0, 0,
		0, 0, -farZ / (farZ - nearZ), -farZ * nearZ / (farZ - nearZ),
		0, 0, -1, 0);
	
	mProjToView = mViewToProj.GetInverted();
	
	mWorldToProj = mViewToProj * mWorldToView;
	mProjToWorld = mWorldToProj.GetInverted();

}

